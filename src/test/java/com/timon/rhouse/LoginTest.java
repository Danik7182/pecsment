package com.timon.rhouse;



import com.timon.rhouse.domain.Consumer;
import com.timon.rhouse.repository.ConsumerRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class LoginTest {


    private static final String CONSUMER_USERNAME = "testconsumer";
    private static final String CONSUMER_NAME = "Consumer";
    private static final String CONSUMER2_NAME = "NewConsumer";
    private static final String USER_ROLE = "USER";
    private static final String CONSUMER_SURNAME="Test";
    private static final String CONSUMER_EMAIL="testconsumer@gmail.com";
    private static final String CONSUMER_PASSWORD= "Consumer123";


    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;


    @Autowired
    ConsumerRepository consumerRepository;
    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }
    /**
     * Get login page view
     *
     * @throws Exception
     */
    @Test
    public void loginPage() throws Exception {
        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);
        mockMvc.perform(get("/consumer/getById/"+newConsumer.getId())).andExpect(status().isOk());
    }

    @Test
    public void login() throws Exception {

        mockMvc.perform(get("/login.xhtml")).andExpect(status().isOk());
    }


    @Test
    public void loginWithUser() throws Exception {

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);
        mockMvc.perform(get("/profConsumerPage.xhtml").with(user(CONSUMER_USERNAME).password(CONSUMER_PASSWORD).roles(USER_ROLE))).andExpect(status().isOk());
    }

    @Test
    public void logOut() throws Exception {
        mockMvc.perform(get("/logout"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/WelcomePage.jsf"));
    }
}
