package com.timon.rhouse;


import com.timon.rhouse.bean.validators.NameValidator;
import com.timon.rhouse.bean.validators.SurnameValidator;
import com.timon.rhouse.bean.validators.UsernameValidator;
import com.timon.rhouse.domain.Consumer;
import com.timon.rhouse.repository.ConsumerRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

@SpringBootTest
public class ValidatorsTest {


    NameValidator nameValidator= new NameValidator();

    SurnameValidator surnameValidator= new SurnameValidator();

    UsernameValidator usernameValidator=new UsernameValidator();

    BeansTest beansTest = new BeansTest();

    private final static String NAME_BAD="s";
    private final static String NAME_GOOD="Daniyar";

    private final static String SURNAME_BAD="s";
    private final static String SURNAME_GOOD="Serikov";

    private final static String USERNAME_BAD_ONE="l";
    private final static String USERNAME_BAD_TWO="danik";
    private final static String USERNAME_GOOD="dserikov07";


    private static final String CONSUMER_USERNAME = "testconsumer";

    @Test(expected = ValidatorException.class)
    public void checkBadName() throws Exception{
        FacesContext context=null;
        UIComponent component=null;
        Object value = NAME_BAD;
        nameValidator.validate(context,component,value);
    }


    @Test()
    public void checkGoodName() throws Exception{
        FacesContext context=null;
        UIComponent component=null;
        Object value = NAME_GOOD;
        nameValidator.validate(context,component,value);
    }

    @Test
    public void checkGoodSurname(){
        FacesContext context=null;
        UIComponent component=null;
        Object value = SURNAME_GOOD;
        surnameValidator.validate(context,component,value);

    }

    @Test(expected = ValidatorException.class)
    public void checkBadSurname(){

        FacesContext context=null;
        UIComponent component=null;
        Object value = SURNAME_BAD;
        surnameValidator.validate(context,component,value);
    }

    @Test(expected = ValidatorException.class)
    public void checkUsernameSame(){

        beansTest.createNewConsumer();

        FacesContext context=null;
        UIComponent component=null;
        Object value = USERNAME_BAD_TWO;
        usernameValidator.validate(context,component,value);
    }

    @Test(expected = ValidatorException.class)
    public void checkUsernameWrong(){

        FacesContext context=null;
        UIComponent component=null;
        Object value = USERNAME_BAD_ONE;
        usernameValidator.validate(context,component,value);
    }

    @Test
    public void checkUsernameRight(){

        FacesContext context=null;
        UIComponent component=null;
        Object value = USERNAME_GOOD;
        usernameValidator.validate(context,component,value);
    }
}
