package com.timon.rhouse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.timon.rhouse.domain.Consumer;
import com.timon.rhouse.domain.File;
import com.timon.rhouse.domain.Flat;
import com.timon.rhouse.domain.Landlord;
import com.timon.rhouse.repository.FlatRepository;
import org.javamoney.moneta.FastMoney;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.zalando.jackson.datatype.money.MoneyModule;

import javax.money.Monetary;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class RESTFlatTest extends AbstractTest {



    private static final String FLAT_TITLE=" New Flat";
    private static final String FLAT_CITY = "Pecs";
    private static final String FLAT_ADDRESS = "Rokus 7A";
    private static final String FLAT_DETAILS="Details for new flat";
    private static final Boolean FLAT_RENTORBUY = true;
    private static final List<File> FLAT_FILES= new ArrayList<>();
    private static final int FLAT_NUMBEROFROOMS=2;
    private static final int FLAT_NUMBEROFBEDS=5;
    private static final int FLAT2_NUMBEROFBEDS=3;
    private static final boolean FLAT_WIFI=true;
    private static final boolean FLAT_RENOVATE = false;
    private static final boolean FLAT_FURNISHED = false;
    private static final Float FLAT_INTERNETSPEED = 40.0f;
    private static final LocalDate FLAT_AVAILABLEFROM= LocalDate.of(2000,06,30);
    private static final BigDecimal FLAT_UTILITIESDECIMAL= new BigDecimal(30000);
    private static final BigDecimal FLAT_PRICEDECIMAL=new BigDecimal(750000);



    @Autowired
    FlatRepository flatRepository;

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void createFlat() throws Exception {
        Landlord landlord = new Landlord();

        String uri = "/flat/create";


        Flat flat = new Flat(FLAT_TITLE,FLAT_CITY,FLAT_ADDRESS,FLAT_PRICEDECIMAL,FLAT_UTILITIESDECIMAL,FLAT_DETAILS,FLAT_NUMBEROFROOMS,FLAT_NUMBEROFBEDS,FLAT_WIFI,FLAT_INTERNETSPEED,FLAT_RENOVATE,FLAT_FURNISHED,FLAT_RENTORBUY,"EUR",
                landlord,FLAT_AVAILABLEFROM);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(flat);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"))).content(requestJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(200, status);
        Flat flat1 = flatRepository.findByTitle(FLAT_TITLE);
        Assert.assertEquals(flat1.getNumberOfBeds(),FLAT_NUMBEROFBEDS);

    }

}
