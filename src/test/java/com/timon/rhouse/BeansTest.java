package com.timon.rhouse;

import com.timon.rhouse.bean.ConsumerBean;
import com.timon.rhouse.bean.FlatBean;
import com.timon.rhouse.bean.LandlordBean;
import com.timon.rhouse.bean.PageBean;
import com.timon.rhouse.domain.Consumer;

import com.timon.rhouse.domain.File;
import com.timon.rhouse.domain.Flat;
import com.timon.rhouse.domain.Landlord;
import com.timon.rhouse.repository.ConsumerRepository;
import com.timon.rhouse.repository.FlatRepository;
import com.timon.rhouse.repository.LandlordRepository;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@SpringBootTest
public class BeansTest {

    private static final String ROLE_USER="ROLE_USER";

    private static final String CONSUMER_USERNAME = "testconsumer";
    private static final String CONSUMER_NAME = "Consumer";
    private static final String CONSUMER_FIRSTLETTER = "C";
    private static final String CONSUMER_SURNAME="Test";
    private static final String CONSUMER_EMAIL="testconsumer@gmail.com";
    private static final String CONSUMER_PASSWORD= "Consumer123";

    private static final String LANDLORD_USERNAME = "testLandlordnew";
    private static final String LANDLORD_NAME = "Landlordnew";
    private static final String LANDLORD_FIRSTLETTER = "L";
    private static final String LANDLORD_SURNAME="Landlordnew";
    private static final String LANDLORD_EMAIL="testLandlord@gmail.comnew";
    private static final String LANDLORD_CONTACTNUMBER="+770295563052";
    private static final String LANDLORD_PASSWORD= "Landlord123new";



    private static final String FLAT_TITLE=" New Flat";
    private static final String FLAT_CITY = "Pecs";
    private static final String FLAT_ADDRESS = "Rokus 7A";
    private static final String FLAT_DETAILS="Details for new flat";
    private static final Boolean FLAT_RENTORBUY = true;
    private static final List<File> FLAT_FILES= new ArrayList<>();
    private static final int FLAT_NUMBEROFROOMS=2;
    private static final int FLAT_NUMBEROFBEDS=5;
    private static final int FLAT2_NUMBEROFBEDS=3;
    private static final boolean FLAT_WIFI=true;
    private static final boolean FLAT_RENOVATE = false;
    private static final boolean FLAT_FURNISHED = false;
    private static final Float FLAT_INTERNETSPEED = 40.0f;
    private static final LocalDate FLAT_AVAILABLEFROM= LocalDate.of(2000,06,30);
    private static final BigDecimal FLAT_UTILITIESDECIMAL= new BigDecimal(30000);
    private static final BigDecimal FLAT_PRICEDECIMAL=new BigDecimal(750000);



    @Autowired
    ConsumerBean consumerBean;

    @Autowired
    LandlordBean landlordBean;

    @Autowired
    FlatBean flatBean;

    @Autowired
    PageBean pageBean;

    @Autowired
    ConsumerRepository consumerRepository;

    @Autowired
    LandlordRepository landlordRepository;

    @Autowired
    FlatRepository flatRepository;



//    FLAT BEAN

    @Test
    @WithMockUser(username = LANDLORD_USERNAME, authorities = { ROLE_USER})
    void createNewFlat(){
        flatRepository.deleteById(flatRepository.findByTitle(FLAT_TITLE).getId());
        this.createNewLandlord();
        Landlord landlord= landlordRepository.findByUsername(LANDLORD_USERNAME);

        landlordRepository.save(landlord);
        flatBean.setTitle(FLAT_TITLE);
        flatBean.setCity(FLAT_CITY);
        flatBean.setAddress(FLAT_ADDRESS);
        flatBean.setPriceDecimal(FLAT_PRICEDECIMAL);
        flatBean.setUtilitiesDecimal(FLAT_UTILITIESDECIMAL);
        flatBean.setDetails(FLAT_DETAILS);
        flatBean.setNumberOfRooms(FLAT_NUMBEROFROOMS);
        flatBean.setNumberOfBeds(FLAT_NUMBEROFBEDS);
        flatBean.setWifi(FLAT_WIFI);
        flatBean.setInternetSpeed(FLAT_INTERNETSPEED);
        flatBean.setRenovated(FLAT_RENOVATE);
        flatBean.setFurnished(FLAT_FURNISHED);
        flatBean.setRentORbuy(FLAT_RENTORBUY);
        flatBean.setLandlord(landlord);
        flatBean.setAvailableFrom(FLAT_AVAILABLEFROM);


        Assert.assertEquals(flatBean.getId(),0);
        flatBean.createFlat(flatBean);
        Flat flat= flatRepository.findByTitle(FLAT_TITLE);
        Assert.assertNotEquals(flatBean.getId(),0);
        Assert.assertEquals(flat.getNumberOfBeds(),FLAT_NUMBEROFBEDS);
    }


    void createFlat(){

        this.createNewLandlord();
        Landlord landlord= landlordRepository.findByUsername(LANDLORD_USERNAME);

        landlordRepository.save(landlord);
        flatBean.setTitle(FLAT_TITLE);
        flatBean.setCity(FLAT_CITY);
        flatBean.setAddress(FLAT_ADDRESS);
        flatBean.setPriceDecimal(FLAT_PRICEDECIMAL);
        flatBean.setUtilitiesDecimal(FLAT_UTILITIESDECIMAL);
        flatBean.setDetails(FLAT_DETAILS);
        flatBean.setNumberOfRooms(FLAT_NUMBEROFROOMS);
        flatBean.setNumberOfBeds(FLAT_NUMBEROFBEDS);
        flatBean.setWifi(FLAT_WIFI);
        flatBean.setInternetSpeed(FLAT_INTERNETSPEED);
        flatBean.setRenovated(FLAT_RENOVATE);
        flatBean.setFurnished(FLAT_FURNISHED);
        flatBean.setRentORbuy(FLAT_RENTORBUY);
        flatBean.setLandlord(landlord);
        flatBean.setAvailableFrom(FLAT_AVAILABLEFROM);
        flatBean.createFlat(flatBean);

    }


    @Test
    void updateFlat(){


        Flat flat = flatRepository.findByTitle(FLAT_TITLE);
        Assert.assertEquals(flat.getNumberOfBeds(),FLAT_NUMBEROFBEDS);
        flat.setNumberOfBeds(FLAT2_NUMBEROFBEDS);
        flatBean.save(null,null,flat);
        flat = flatRepository.findByTitle(FLAT_TITLE);
        Assert.assertNotEquals(flat.getNumberOfBeds(),FLAT_NUMBEROFBEDS);
    }

    @Test
    void loadDataFlat() throws Exception {
        List<Flat> listOfFlats= flatBean.getListOfFlats();
        Assert.assertNull(listOfFlats);
        flatBean.loadData(0,10);
        listOfFlats= flatBean.getListOfFlats();
        Assert.assertNotNull(listOfFlats);
    }




//    CONSUMER BEAN

    @Test
    void createNewConsumer(){
        consumerRepository.deleteByEmail(CONSUMER_EMAIL);
        consumerBean.setUsername(CONSUMER_USERNAME);
        consumerBean.setName(CONSUMER_NAME);
        consumerBean.setSurname(CONSUMER_SURNAME);
        consumerBean.setEmail(CONSUMER_EMAIL);
        consumerBean.setPassword(CONSUMER_PASSWORD);
        consumerBean.setRole(ROLE_USER);
        consumerBean.createNewConsumer(consumerBean);

        Assert.assertEquals(CONSUMER_EMAIL,consumerRepository.findByEmail(CONSUMER_EMAIL).getEmail());
    }

    @Test
    @WithMockUser(username = CONSUMER_USERNAME, authorities = { ROLE_USER})
    void getLoggedUserConsumer(){
        this.createNewConsumer();
        Consumer consumer =consumerBean.getLoggedUser();
        Assert.assertEquals(CONSUMER_USERNAME,consumer.getUsername());
        Assert.assertNotNull(consumer);
    }

    @Test
    @WithMockUser(username = CONSUMER_USERNAME, authorities = { ROLE_USER})
    void getFirstLetterOfNameConsumer(){
        this.createNewConsumer();
        String letter = consumerBean.getFirstLetterOfName();
        Assert.assertEquals(CONSUMER_FIRSTLETTER,letter);
    }


    @Test
    @WithMockUser(username = CONSUMER_USERNAME, authorities = { ROLE_USER})
    void addFlatToFavouriteList() throws InterruptedException {

        Consumer consumer=consumerRepository.findByUsername(CONSUMER_USERNAME);
        Assert.assertEquals(consumer.getFavouriteFlats().size(),0);
        consumerBean.addFlatToFavouriteList(flatRepository.findByTitle(FLAT_TITLE).getId());
        consumer=consumerRepository.findByUsername(CONSUMER_USERNAME);
        Assert.assertEquals(consumer.getFavouriteFlats().size(),1);

    }


    @Test
    @WithMockUser(username = CONSUMER_USERNAME, authorities = { ROLE_USER})
    void loadFavouriteFLats() throws InterruptedException {

        this.createNewConsumer();
        this.createFlat();
        consumerBean.addFlatToFavouriteList(flatRepository.findByTitle(FLAT_TITLE).getId());
        Assert.assertEquals(consumerBean.getFavouriteFlats(),null);
        consumerBean.loadFavouriteFLats();
        Assert.assertNotNull(consumerBean.getFavouriteFlats());

    }


    @Test
    @WithMockUser(username = CONSUMER_USERNAME, authorities = { ROLE_USER})
    void deleteFlatFromFavouriteList() throws InterruptedException {


        Consumer consumer=consumerRepository.findByUsername(CONSUMER_USERNAME);




        consumerBean.deleteFlatFromFavouriteList(consumer.getFavouriteFlats().get(0));
        consumer=consumerRepository.findByUsername(CONSUMER_USERNAME);
        Assert.assertEquals(consumer.getFavouriteFlats().size(),0);

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
    }


//    LANDLORD BEAN
    @Test
    void createNewLandlord(){


        landlordRepository.deleteByEmail(LANDLORD_EMAIL);
        landlordBean.setUsername(LANDLORD_USERNAME);
        landlordBean.setName(LANDLORD_NAME);
        landlordBean.setSurname(LANDLORD_SURNAME);
        landlordBean.setContactNumber(LANDLORD_CONTACTNUMBER);
        landlordBean.setEmail(LANDLORD_EMAIL);
        landlordBean.setPassword(LANDLORD_PASSWORD);
        landlordBean.createNewLandlord(landlordBean);
        Landlord landlord=landlordRepository.findByUsername(LANDLORD_USERNAME);
        Assert.assertEquals(landlord.getUsername(),LANDLORD_USERNAME);
    }

    @Test
    @WithMockUser(username = LANDLORD_USERNAME, authorities = { ROLE_USER})
    void getFirstLetterOfNameLandlord() throws Exception {

        Landlord landlord=landlordRepository.findByUsername(LANDLORD_USERNAME);
        Assert.assertEquals(landlordBean.getFirstLetterOfName(),LANDLORD_FIRSTLETTER);
    }

    @Test
    void loadMyFlatsLandlord(){
        List<Flat> flats= landlordBean.getLandlordsFlatList();
        Assert.assertNull(flats);
        Landlord landlord=landlordRepository.findByUsername(LANDLORD_USERNAME);
        landlordBean.loadMyFlats(landlord.getId());
        flats=landlordBean.getLandlordsFlatList();
        Assert.assertNotNull(flats);

    }

    @Test
    void loadDataLandlord() throws Exception {
        List<Flat> flats= landlordBean.getFlatList();
        Assert.assertNull(flats);
        landlordBean.loadData(0,10);
        flats= landlordBean.getFlatList();
        Assert.assertNotNull(flats);
    }


    @Test
    @WithMockUser(username = LANDLORD_USERNAME, authorities = { ROLE_USER})
    void getLoggedUserLandlord() throws Exception {

        this.createNewLandlord();
       List<Flat> flats= landlordBean.getFlatList();
        Assert.assertNull(flats);
       Landlord landlord= landlordBean.getLoggedUser();
       Assert.assertEquals(landlord.getUsername(),LANDLORD_USERNAME);
        flats= landlordBean.getFlatList();
        Assert.assertNotNull(flats);
    }



//    PAGE BEAN


    @Test
    @WithMockUser(username = LANDLORD_USERNAME, authorities = { ROLE_USER})
    void isUser() throws Exception {

        Assert.assertEquals(pageBean.isUser(),true);

    }


    @Test
    @WithMockUser(username = LANDLORD_USERNAME, authorities = { ROLE_USER})
    void isConsumer() throws Exception {

        Assert.assertEquals(pageBean.isConsumer(),false);
    }

    @Test
    @WithMockUser(username = LANDLORD_USERNAME, authorities = { ROLE_USER})
    void logout(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Assert.assertNotNull(auth);
        pageBean.logout();
        auth = SecurityContextHolder.getContext().getAuthentication();
        Assert.assertNull(auth);


    }

}
