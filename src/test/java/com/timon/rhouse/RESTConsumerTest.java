package com.timon.rhouse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.timon.rhouse.domain.Consumer;
import com.timon.rhouse.repository.ConsumerRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;

public class RESTConsumerTest extends AbstractTest {



    private static final String CONSUMER_USERNAME = "testconsumer";
    private static final String CONSUMER_NAME = "Consumer";
    private static final String CONSUMER2_NAME = "NewConsumer";
    private static final String USER_ROLE = "ROLE_USER";
    private static final String CONSUMER_SURNAME="Test";
    private static final String CONSUMER_EMAIL="testconsumer@gmail.com";
    private static final String CONSUMER_PASSWORD= "Consumer123";

    @Autowired
    ConsumerRepository consumerRepository;

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void createConsumer() throws Exception {

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        String uri = "/consumer/create";
        Consumer consumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);



        String inputJson = super.mapToJson(consumer);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(200, status);
        Consumer consumer1 = consumerRepository.findByUsername(CONSUMER_USERNAME);
        Assert.assertEquals(consumer1.getEmail(),CONSUMER_EMAIL);


    }

    @Test
    public void updateConsumer() throws Exception {

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);
        Consumer consumer = consumerRepository.findByUsername(CONSUMER_USERNAME);

        long id=consumer.getId();
        String uri = "/consumer/update/"+consumer.getId();
        consumer.setName(CONSUMER2_NAME);

        String inputJson = super.mapToJson(consumer);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(200, status);
        Consumer consumer1 = consumerRepository.findByUsername(CONSUMER_USERNAME);
        Assert.assertEquals(consumer1.getName(),CONSUMER2_NAME);

    }


    @Test
    public void getAllConsumer() throws Exception {
        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        String uri = "/consumer/getAll";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(204, status);

        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);

        mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        status = mvcResult.getResponse().getStatus();

        Assert.assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();

        List<Consumer> consumers = JsonPath.parse(content).read("$[*]");
//        Assert.assertTrue(consumerList.size() > 0);
        Assert.assertTrue(consumers.size()>0);
    }


    @Test
    public void getConsumerById() throws Exception {

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);
        Consumer consumer = consumerRepository.findByUsername(CONSUMER_USERNAME);

        long id=consumer.getId();
        String uri = "/consumer/getById/"+consumer.getId();

        String inputJson = super.mapToJson(consumer);


        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        List<Consumer> consumers = JsonPath.parse(content).read("$[*]");

        Assert.assertEquals(consumers.get(5),CONSUMER_NAME);

    }

    @Test
    public void getConsumerByEmail() throws Exception {

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);


        String uri = "/consumer/getByEmail/"+CONSUMER_EMAIL;


        String inputJson = super.mapToJson(newConsumer);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        List<Consumer> consumers = JsonPath.parse(content).read("$[*]");

        Assert.assertEquals(consumers.get(7),CONSUMER_EMAIL);

    }

    @Test
    public void getConsumerByUsername() throws Exception{

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);


        String uri = "/consumer/getByUsername/"+CONSUMER_USERNAME;


        String inputJson = super.mapToJson(newConsumer);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        List<Consumer> consumers = JsonPath.parse(content).read("$[*]");

        Assert.assertEquals(consumers.get(7),CONSUMER_EMAIL);
    }


    @Test
    public void getConsumerByName() throws Exception{

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);


        String uri = "/consumer/getByName/"+CONSUMER_NAME;


        String inputJson = super.mapToJson(newConsumer);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        LinkedHashMap<String, String> contents = JsonPath.parse(content).read("$[0]");

        String email =contents.get("email");
        Assert.assertEquals(email,CONSUMER_EMAIL);

    }

    @Test
    public void getConsumerBySurname() throws Exception{

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);


        String uri = "/consumer/getBySurname/"+CONSUMER_SURNAME;


        String inputJson = super.mapToJson(newConsumer);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        LinkedHashMap<String, String> contents = JsonPath.parse(content).read("$[0]");

        String email =contents.get("email");
        Assert.assertEquals(email,CONSUMER_EMAIL);

    }

    @Test
    public void getConsumerByNameANDsurnamae() throws Exception{

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);


        String uri = "/consumer/getByNameAndSurname/"+CONSUMER_NAME + "/" +CONSUMER_SURNAME;


        String inputJson = super.mapToJson(newConsumer);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        LinkedHashMap<String, String> contents = JsonPath.parse(content).read("$[0]");

        String email =contents.get("email");
        Assert.assertEquals(email,CONSUMER_EMAIL);

    }


    @Test
    public void deleteAllConsumers() throws Exception{

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);


        String uri = "/consumer/deleteAll";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();


        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(204, status);

    }


    @Test
    public void deleteByIdConsumers() throws Exception{

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);


        String uri = "/consumer/deleteById/"+newConsumer.getId();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();


        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(204, status);

    }

    @Test
    public void deleteByUsername() throws Exception{

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);


        String uri = "/consumer/deleteByUsername/"+newConsumer.getUsername();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();


        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(204, status);

    }

    @Test
    public void deleteByEmail() throws Exception{

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);


        String uri = "/consumer/deleteByEmail/"+newConsumer.getEmail();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();


        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(204, status);

    }


    @Test
    public void deleteAllByName() throws Exception{

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);


        String uri = "/consumer/deleteAllByName/"+newConsumer.getName();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();


        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(204, status);

    }


    @Test
    public void deleteAllBySurname() throws Exception{

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);


        String uri = "/consumer/deleteAllBySurname/"+newConsumer.getSurname();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();


        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(204, status);

    }

    @Test
    public void deleteAllByNameAndSurname() throws Exception{

        consumerRepository.deleteByUsername(CONSUMER_USERNAME);
        Consumer newConsumer = new Consumer(CONSUMER_USERNAME, CONSUMER_NAME,CONSUMER_SURNAME, CONSUMER_EMAIL, CONSUMER_PASSWORD,USER_ROLE);
        consumerRepository.save(newConsumer);


        String uri = "/consumer/deleteAllByNameAndSurname/"+newConsumer.getName()+"/"+newConsumer.getSurname();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();


        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(204, status);

    }
}
