package com.timon.rhouse;

import com.timon.rhouse.domain.Consumer;
import com.timon.rhouse.domain.Landlord;
import com.timon.rhouse.repository.ConsumerRepository;
import com.timon.rhouse.repository.LandlordRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;

public class RESTLandlordTest extends AbstractTest {

    private static final String LANDLORD_USERNAME = "testLandlord";
    private static final String LANDLORD_NAME = "Landlord";
    private static final int LANDLORD_MONEY = 10000;
    private static final String LANDLORD_CURRENCY="EUR";
    private static final String LANDLORD_SURNAME="Landlord";
    private static final String LANDLORD_EMAIL="testLandlord@gmail.com";
    private static final String LANDLORD_CONTACTNUMBER="+77029556305";
    private static final String LANDLORD_PASSWORD= "Landlord123";
    private static final String ROLE_USER= "ROLE_USER";



    @Autowired
    LandlordRepository landlordRepository;

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void createLandlord() throws Exception {

        landlordRepository.deleteByEmail(LANDLORD_EMAIL);
        String uri = "/landlord/create";

        Landlord landlord= new Landlord(LANDLORD_USERNAME,LANDLORD_NAME,LANDLORD_SURNAME,LANDLORD_EMAIL,LANDLORD_CONTACTNUMBER,LANDLORD_PASSWORD,new BigDecimal(LANDLORD_MONEY),LANDLORD_CURRENCY,ROLE_USER);

        String inputJson = super.mapToJson(landlord);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(200, status);
        Landlord landlord1 = landlordRepository.findByUsername(LANDLORD_USERNAME);
        Assert.assertEquals(landlord1.getEmail(),LANDLORD_EMAIL);


    }
}
