package com.timon.rhouse.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;


@Entity(name = File.TBL_NAME)
public class File extends AbstractEntity<Long>{
    /**
     * TBL_NAME is name of Table to store files
     */
    public static final String TBL_NAME="images";

    /**
     * FLD_FILENAME is name of file column
     */
    public static final String FLD_FILENAME="fileName";
    /**
     * FLD_DATA is name of column for data, bytes
     */
    public static final String FLD_DATA="data";
    /**
     * FLD_TYPE is name of column for type of file
     */
    public static final String FLD_TYPE="type";

    /**
     * FLD_FLAT is name of column for Flat object
     */
    public static final String FLD_FLAT="flats";


    /**
     * Column filename is name of file
     * Type: String
     * Can be null and NOT unique
     */
    @Column(name = FLD_FILENAME,unique = false,nullable = true)
    private String fileName;

    /**
     * Column type is type of file
     * Type: String
     * Can be null and NOT unique
     */
    @Column(name = FLD_TYPE,unique = false,nullable = true)
    private String type;

    /**
     * Column data is bytes of file
     * Type: byte[]
     * Can be null and NOT unique
     */
    @Column(name = FLD_DATA,unique = false,nullable = true)
    private byte[] data;

    /**
     * Column flats is Flat of file
     * ManyToOne - many files can have one flat, each flat can have multiple files
     * Type: Flat Object
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = FLD_FLAT)
    @JsonBackReference(value = "flats")
    private Flat flats;


    /**
     * Default Empty Constructor
     */
    public File(){

    }

    /**
     * Constructor with 4 parameters
     * @param fileName is name of File.Type:String
     * @param type is type of File.Type:String
     * @param data is bytes of File.Type:byte[]
     * @param flat is Flat of File.Type:Flat
     */
    public File(String fileName,String type,byte[] data,Flat flat){
        this.fileName=fileName;
        this.type=type;
        this.data=data;
        this.flats=flat;
    }

    /**
     * Getter for {@link File#flats}
     * @return Flat object
     */
    public Flat getFlats() {
        return flats;
    }

    /**
     * Setter for {@link File#flats}
     * @param flats is Flat object
     */
    public void setFlats(Flat flats) {
        this.flats = flats;
    }

    /**
     * Getter for {@link File#type}
     * @return String type of file
     */
    public String getType() {
        return type;
    }

    /**
     * Setter for {@link File#type}
     * @param type is String type of file
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Getter for {@link File#fileName}
     * @return String name of file
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Setter for {@link File#fileName}
     * @param fileName is name of  file
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Getter for {@link File#data}
     * @return byte[] of file(data)
     */
    public byte[] getData() {
        return data;
    }

    /**
     * Setter for {@link File#data}
     * @param data is bytes of file
     */
    public void setData(byte[] data) {
        this.data = data;
    }
}
