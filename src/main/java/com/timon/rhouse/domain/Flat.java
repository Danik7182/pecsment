package com.timon.rhouse.domain;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.javamoney.moneta.FastMoney;

import javax.money.Monetary;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.*;

@Entity(name = Flat.TBL_NAME)
public class Flat extends AbstractEntity<Long> {

    /**
     * TBL_NAME is name of Table where Flat.class entities will be stored in database
     */
    public static final String TBL_NAME="flats";


    /**
     * FLD_LANDLORDS is name of column for {@link Flat#landlord}
     */
    public static final String FLD_LANDLORDS="landlord";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#title}
     */
    public static final String FLD_TITLE="title";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#city}
     */
    public static final String FLD_CITY="city";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#address}
     */
    public static final String FLD_ADDRESS="address";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#price}
     */
    public static final String FLD_PRICE="price";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#utilities}
     */
    public static final String FLD_UTILITIES="utilities";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#total}
     */
    public static final String FLD_TOTAL="total";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#details}
     */
    public static final String FLD_DETAILS="details";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#rentORbuy}
     */
    public static final String FLD_RENTORBUY="rentORbuy";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#numberOfRooms}
     */
    public static final String FLD_ROOMS="numberOfRooms";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#numberOfBeds}
     */
    public static final String FLD_BEDS="numberOfBeds";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#wifi}
     */
    public static final String FLD_WIFI="wifi";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#furnished}
     */
    public static final String FLD_FURNISHED="furnished";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#renovated}
     */
    public static final String FLD_RENOVATED="renovated";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#internetSpeed}
     */
    public static final String FLD_INTERNETSPEED="internetSpeed";
    /**
     * FLD_LANDLORDS is name of column for {@link Flat#availableFrom}
     */
    public static final String FLD_AVAILABLEDATE="availableFrom";


    /**
     * Value name: title
     * Type: String
     * Is: title of Flat
     * Can't be null and NOT unique
     */
    @Column(name = FLD_TITLE,unique = false,nullable = false)
    @NotNull(message = "username can't be null")
    @Size(min = 2,max =90,message = "Length of username can be 2-30")
    private String title;

    /**
     * Value name: city
     * Type: String
     * Is: city name of Flat
     * Can't be null and NOT unique
     */
    @Column(name = FLD_CITY,unique = false,nullable = false)
    @NotNull(message = "city of flat can't be null")
    @Size(min = 2,max =30,message = "Length of city can be 2-30")
    private String city;

    /**
     * Value name: address
     * Type: String
     * Is: address of Flat
     * Can't be null and NOT unique
     */
    @Column(name = FLD_ADDRESS,unique = false,nullable = false)
    @NotNull(message = "address of flat can't be null")
    @Size(min = 2,max =30,message = "address can be 2-30")
    private String address;

    /**
     * Value name: price
     * Type: FastMoney object
     * Is: price of Flat
     * Can't be null and NOT unique
     */
    @Column(name = FLD_PRICE,unique = false,nullable = false)
    @NotNull(message = "price can't be null")
    private FastMoney price;

    /**
     * Value name: utilities
     * Type: FastMoney object
     * Is: utilities cost of Flat
     * Can't be null and NOT unique
     */
    @Column(name = FLD_UTILITIES,unique = false,nullable = false)
    @NotNull(message = "utilities can't be null")
    private FastMoney utilities;

    /**
     * Value name: total
     * Type: FastMoney object
     * Is: total price of Flat
     * Can't be null and NOT unique
     */
    @Column(name = FLD_TOTAL,unique = false,nullable = false)
    @NotNull(message = "total can't be null")
    private FastMoney total;

    /**
     * Value name: details
     * Type: String
     * Is: Details of Flat
     * Can't be null and NOT unique
     */
    @Column(name = FLD_DETAILS,unique = false,nullable = false)
    @NotNull(message = "details  can't be null")
    @Size(min = 2,max =1000,message = "Details can be 2-1000")
    private String details;

    /**
     * Value name: rentORbuy
     * Type: Boolean
     * Is: Flat for rent or not. TRUE - Rent; FALSE - Buy
     * Can't be null and NOT unique
     */
    @Column(name = FLD_RENTORBUY,unique = false,nullable = false)
    @NotNull(message = "rent or buy  boolean can't be null")
    private Boolean rentORbuy;


    /**
     * Value name: files
     * Type: List File - is list of files
     * Is: List of files of Flat
     * Can be null
     * OneToMany connection with File.class - one flat can have multiple files
     */

    @OneToMany(mappedBy = File.FLD_FLAT,orphanRemoval = true,fetch = FetchType.EAGER)
    @JsonManagedReference(value = "files")
    private List<File> files;


    /**
     * Value name: landlord
     * Type: Landlord
     * Is: landlord of Flat, owner of the flat
     * ManyToOne connection with Landlord.class. Many flats can have one Landlord
     */
    @JsonBackReference(value = "landlord")
    @ManyToOne(fetch = FetchType.LAZY,cascade = {
            CascadeType.MERGE
    })
    @JoinColumn(name=FLD_LANDLORDS)
    private Landlord landlord;

    /**
     * Value name: numberOfRooms
     * Type: int
     * Is: Number of rooms in the Flat
     * Can't be null and NOT unique
     */
    @Column(name = FLD_ROOMS,unique = false,nullable = false)
    @NotNull(message = "rooms number can't be null")
    private int numberOfRooms;

    /**
     * Value name: numberOfBeds
     * Type: int
     * Is: Number of Beds in the Flat
     * can't be null
     */
    @Column(name = FLD_BEDS,nullable = false)
    private int numberOfBeds;

    /**
     * Value name: wifi
     * Type: boolean
     * Is: showing Flat has wifi or not
     * Can't be null
     */
    @Column(name = FLD_WIFI,nullable = false)
    @NotNull(message = " wifi boolean can't be null")
    private boolean wifi;

    /**
     * Value name: renovated
     * Type: boolean
     * Is: is Flat renovated or not
     * Can't be null
     */
    @Column(name = FLD_RENOVATED,nullable = false)
    @NotNull(message = "renovated boolean can't be null")
    private boolean renovated;

    /**
     * Value name: furnished
     * Type: boolean
     * Is: Is Flat furnished or Not
     * Can't be null
     */
    @Column(name = FLD_FURNISHED,nullable = false)
    @NotNull(message = "furnished  boolean can't be null")
    private boolean furnished;

    /**
     * Value name: internetSpeed
     * Type: float
     * Is: Speed on internet in the  Flat
     * Can be null
     */
    @Column(name = FLD_INTERNETSPEED)
    private float internetSpeed;

    /**
     * Value name: availableFrom
     * Type: LocalDate
     * Is: Date when Flat is available
     * Format: year.mm.dd
     */
    @Column(name = FLD_AVAILABLEDATE,nullable = true,columnDefinition = "DATE")
    @NotNull(message = "availableFrom can't be null")
    private LocalDate availableFrom;


    /**
     * normalPrice is price of Flat in string format
     */
    private String normalPrice;
    /**
     * normalUtilities is utilities cost of Flat in string format
     */
    private String normalUtilities;

    /**
     * Empty Constructor
     */
    public Flat() {
    }

    /**
     * Constructor for Flat with 16 parameters
     * @param title is Title of Flat. Type: String
     * @param city  is City name of Flat. Type: String
     * @param address  is Address of Flat. Type: String
     * @param price  is Price of Flat.In constructor body -FastMoney is creating from BigDecimal Type: BigDecimal
     * @param utilities  is Utilities cost of Flat.In constructor body- FastMoney is creating from BigDecimal Type: BigDecimal
     * @param details  is Details of Flat. Type: String
     * @param numberOfRooms  is Number of rooms of Flat. Type: int
     * @param numberOfBeds  is Number of Beds of Flat. Type: int
     * @param wifi  is -Wifi existed in  Flat or not. Type: boolean
     * @param internetSpeed  is Float of Flat. Type: float
     * @param renovated  is Renovated  Flat. Type: boolean
     * @param furnished  is Furnished  Flat. Type: boolean
     * @param rentORbuy  is   Flat for rent(true) or sell(false). Type: boolean
     * @param currencyUnit  is Currency of Flat. Type: String
     * @param landlord  is Landlord(owner) of Flat. Type: Landlord
     * @param availableFrom  is Date when  Flat is available. Type: LocalDate
     * this.total - is adding FastMoney Price and Fast Money Utilities
     * this.files - createing new Array List
     */
    public Flat(String title, String city, String address, BigDecimal price,
                BigDecimal utilities, String details, int numberOfRooms ,int numberOfBeds ,
                boolean wifi,float internetSpeed,boolean renovated,
                boolean furnished,Boolean rentORbuy,String currencyUnit,Landlord landlord,LocalDate availableFrom ){

        this.internetSpeed=internetSpeed;
        this.availableFrom=availableFrom;
        this.numberOfBeds=numberOfBeds;
        this.numberOfRooms=numberOfRooms;
        this.wifi=wifi;
        this.renovated=renovated;
        this.furnished=furnished;
        this.title=title;
        this.city=city;
        this.address=address;
        this.price=FastMoney.of(price, Monetary.getCurrency(currencyUnit));
        this.utilities=FastMoney.of(utilities, Monetary.getCurrency(currencyUnit));
        this.total=this.price.add(this.utilities);
        this.details=details;
        this.rentORbuy=rentORbuy;
        this.landlord=landlord;
        files=new ArrayList<>();
    }

    /**
     * Getter for {@link Flat#files}
     * @return files(images) of Flat
     */
    public List<File> getFiles() {
        return files;
    }

    /**
     * Setter for {@link Flat#files}
     * @param files is List of files(images)
     */
    public void setFiles(List<File> files) {
        this.files = files;
    }

    /**
     * Getter for {@link Flat#internetSpeed}
     * @return speed of Internet in the Flat
     */
    public float getInternetSpeed() {
        return internetSpeed;
    }

    /**
     * Setter for {@link Flat#internetSpeed}
     * @param internetSpeed is speed of Flat
     */
    public void setInternetSpeed(float internetSpeed) {
        this.internetSpeed = internetSpeed;
    }

    /**
     * Getter for {@link Flat#availableFrom}
     * @return date when Flat is available
     */
    public LocalDate getAvailableFrom() {
        return availableFrom;
    }

    /**
     * Setter for {@link Flat#availableFrom}
     * @param availableFrom is LocalDate for setting date .format: year.mm.dd
     */
    public void setAvailableFrom(LocalDate availableFrom) {
        this.availableFrom = availableFrom;
    }

    /**
     * Getter for {@link Flat#numberOfRooms}
     * @return number of rooms in the Flat
     */
    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    /**
     * Setter for {@link Flat#numberOfRooms}
     * @param numberOfRooms is count of rooms in the Flat
     */
    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    /**
     * Getter for {@link Flat#numberOfBeds}
     * @return number of beds in the Flat
     */
    public int getNumberOfBeds() {
        return numberOfBeds;
    }

    /**
     * Setter for {@link Flat#numberOfBeds}
     * @param numberOfBeds is number of beds in the Flat
     */
    public void setNumberOfBeds(int numberOfBeds) {
        this.numberOfBeds = numberOfBeds;
    }

    /**
     * Getter if wifi exist or not for {@link Flat#wifi}
     * @return boolean if wifi exist
     */
    public boolean isWifi() {
        return wifi;
    }

    /**
     * Setter for {@link Flat#wifi}
     * @param wifi boolean if wifi exist or not (true/false)
     */
    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    /**
     * Getter for {@link Flat#renovated}
     * @return boolean if Flat is renovate or not (true/false)
     */
    public boolean isRenovated() {
        return renovated;
    }

    /**
     * Setter for {@link Flat#renovated}
     * @param renovated is boolean if flat renovated or not
     */
    public void setRenovated(boolean renovated) {
        this.renovated = renovated;
    }

    /**
     * Getter for {@link Flat#furnished}
     * @return boolean if Flat is furnished or not (true/false)
     */
    public boolean isFurnished() {
        return furnished;
    }

    /**
     * Setter for {@link Flat#furnished}
     *@param furnished is boolean if flat furnished or not
     */
    public void setFurnished(boolean furnished) {
        this.furnished = furnished;
    }

    /**
     * Getter for {@link Flat#title}
     * @return title of Flat
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter for {@link Flat#title}
     * @param title is title of Flat , type:String
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter for {@link Flat#city}
     * @return city name of Flat
     */
    public String getCity() {
        return city;
    }

    /**
     * Setter for {@link Flat#city}
     * @param city is name of city of Flat
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Getter for {@link Flat#address}
     * @return returning  String address of Flat
     */
    public String getAddress() {
        return address;
    }

    /**
     * Setter for {@link Flat#address}
     * @param address is setting address of Flat
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Getter for {@link Flat#price}
     * @return FastMoney object with prices details of Flat
     */
    public FastMoney getPrice() {
        return price;
    }


    /**
     * Setter for {@link Flat#price}
     * @param price is FastMoney object to change price of Flat
     */
    public void setPrice(FastMoney price) {
        this.price = price;
    }

    /**
     * Getter for {@link Flat#utilities}
     * @return Fastmoney object with utilities cost of Flat
     */
    public FastMoney getUtilities() {
        return utilities;
    }

    /**
     * Setter for {@link Flat#utilities}
     * @param utilities is FastMoney object to change utilities cost of Flat
     */
    public void setUtilities(FastMoney utilities) {
        this.utilities = utilities;
    }

    /**
     * Getter for {@link Flat#total}
     * @return FastMoney object with total price of Flat
     */
    public FastMoney getTotal() {
        return total;
    }


    /**
     * Setter for {@link Flat#total}
     * @param total is FastMoney object to set total price of Flat
     */
    public void setTotal(FastMoney total) {
        this.total = total;
    }

    /**
     * Getter for {@link Flat#details}
     * @return String details of Flat
     */
    public String getDetails() {
        return details;
    }


    /**
     * Setter for {@link Flat#details}
     * @param details is String to change details of Flat
     */
    public void setDetails(String details) {
        this.details = details;
    }

    /**
     * Getter for {@link Flat#rentORbuy}
     * @return Boolean if flat for rent or sell
     */
    public Boolean getRentORbuy() {
        return rentORbuy;
    }

    /**
     * Setter for {@link Flat#rentORbuy}
     * @param rentORbuy is Boolean to change if flat for rent or sell
     */
    public void setRentORbuy(Boolean rentORbuy) {
        this.rentORbuy = rentORbuy;
    }

    /**
     * Getter for {@link Flat#landlord}
     * @return Landlord object, owner of Flat
     */
    public Landlord getLandlord() {
        return landlord;
    }

    /**
     * Setter for {@link Flat#landlord}
     * @param landlord is Landlord object to set owner of Flat
     */
    public void setLandlord(Landlord landlord) {
        this.landlord = landlord;
    }

    /**
     * Getter for {@link Flat#normalPrice}
     * @return String taken from {@link Flat#price} object
     */
    public String getNormalPrice() {
        Number value= this.getPrice().getNumber();
        String s = new DecimalFormat("0.####").format(Double.parseDouble(String.valueOf(value)));
        return s;
    }


    /**
     * Setter for {@link Flat#normalPrice}
     * @param normalPrice String to change normal price and change {@link Flat#price}
     */
    public void setNormalPrice(String normalPrice) {
        BigDecimal price = new BigDecimal(normalPrice);
        this.price=FastMoney.of(price, Monetary.getCurrency("EUR"));

        this.normalPrice=normalPrice;
    }

    /**
     * Getter for {@link Flat#normalUtilities}
     * @return String taken from {@link Flat#utilities} object
     */
    public String getNormalUtilities() {
        Number value= this.getUtilities().getNumber();
        String s = new DecimalFormat("0.####").format(Double.parseDouble(String.valueOf(value)));
        return s;
    }

    /**
     * Setter for {@link Flat#normalUtilities}
     * @param normalUtilities String to change normal utilities and change {@link Flat#utilities}
     */
    public void setNormalUtilities(String normalUtilities) {
        BigDecimal price = new BigDecimal(normalUtilities);
        this.utilities=FastMoney.of(price, Monetary.getCurrency("EUR"));
        this.normalUtilities = normalUtilities;
    }

    /**
     * Getter for normal Total
     * @return getting in String format the number of Total FastMoney object
     */
    public String getNormalTotal(){

        Number value= this.getTotal().getNumber();
        String s = new DecimalFormat("0.####").format(Double.parseDouble(String.valueOf(value)));
        return s;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flat)) return false;
        if (!super.equals(o)) return false;
        Flat flat = (Flat) o;
        return numberOfRooms == flat.numberOfRooms &&
                numberOfBeds == flat.numberOfBeds &&
                wifi == flat.wifi &&
                renovated == flat.renovated &&
                furnished == flat.furnished &&
                Float.compare(flat.internetSpeed, internetSpeed) == 0 &&
                title.equals(flat.title) &&
                city.equals(flat.city) &&
                address.equals(flat.address) &&
                price.equals(flat.price) &&
                utilities.equals(flat.utilities) &&
                total.equals(flat.total) &&
                details.equals(flat.details) &&
                rentORbuy.equals(flat.rentORbuy) &&
                landlord.equals(flat.landlord);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), title, city, address, price, utilities, total, details, rentORbuy, landlord, numberOfRooms, numberOfBeds, wifi, renovated, furnished, internetSpeed, availableFrom);
    }
}
