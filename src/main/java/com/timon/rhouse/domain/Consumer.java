package com.timon.rhouse.domain;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import java.util.ArrayList;
import java.util.List;

@Entity(name = Consumer.TBL_NAME)
public class Consumer extends AbstractClient {


    /**
     * TBL_NAME is name of Table where will be stored Consumer class entities
     */
    public static final String TBL_NAME="consumers";

    /**
     * List of favourite flats id
     * Type: List Long - is list of favourite flats
     */
      @ElementCollection(fetch= FetchType.EAGER)
      private List<Long> favouriteFlats;


    /**
     *
     * Constructor for Consumer with 6 parameters
     * @param username is taken from super class. Username of Consumer.Type:String
     * @param name is taken from super class. Name of Consumer.Type:String
     * @param surname is taken from super class. Surname of Consumer.Type:String
     * @param email is taken from super class. Email of Consumer.Type:String
     * @param password is taken from super class. Password of Consumer.Type:String
     * @param roles is taken from super class. Roles of Consumer.Type:String
     * When constructor calls it assign also new ArrayList for {@link Consumer#favouriteFlats}
     */
    public Consumer(String username, String name,String surname, String email, String password,String roles) {
        super(username, name,surname, email, password,roles);
        this.favouriteFlats=new ArrayList<>();
    }


    /**
     * Empty Constructor
     */
    public Consumer() {
    }

    /**
     * Getter for {@link Consumer#favouriteFlats}
     * @return list of ids of favourite flats
     */
    public List<Long> getFavouriteFlats() {
        return favouriteFlats;
    }

    /**
     * Setter for {@link Consumer#favouriteFlats}
     * @param favouriteFlats is List of ids of favourite flats
     */
    public void setFavouriteFlats(List<Long> favouriteFlats) {
        this.favouriteFlats = favouriteFlats;
    }
}
