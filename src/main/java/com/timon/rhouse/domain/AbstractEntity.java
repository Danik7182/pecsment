package com.timon.rhouse.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@MappedSuperclass
public class AbstractEntity<ID extends Serializable> implements Serializable {


    /**
     * FLD_IS is column name for ids
     */
    public static final String FLD_ID="id";

    /**
     * Id is id of any client who extend AbstractEntity
     */
    @Id
    @Column(name = FLD_ID, unique = true, updatable = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private ID id;

    /**
     *  version is for version of each entity
     * by default is 0 if entity created and changed version will increase
     */
    @Version
    private Integer version;


    /**
     *  created every entity will have date and time when it created
     */
    @CreationTimestamp
    private Date creationDate;


    /**
     * created every entity will have date and time when it changed last time
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @UpdateTimestamp
    private LocalDateTime updated;



    /**
     * Empty default  constructor for AbstarctEntity class
     */
    public AbstractEntity() {

    }


    /**
     * Constructor with ID parameter
     * @param id is id of entity
     */
    public AbstractEntity(ID id){
        this.id=id;
    }

    /**
     *
     * @return when the entity was created in database
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     *
     * @param creationDate is to set in date format the Entity's date when created in database
     */

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

  

    /**
     *
     * @return id of Entity
     */
    public ID getId() {
        return id;
    }


    /**
     *
     * @param id to set if of Entity
     */
    public void setId(ID id) {
        this.id = id;
    }


    /**
     *
     * @return version of Entity in databse
     */
    public Integer getVersion() {
        return version;
    }


    /**
     *
     * @param version setting version of Entity in dabase in Integer format
     */
    public void setVersion(Integer version) {
        this.version = version;
    }



    /**
     *
     * @return the date when Entity was modified last time in DataBase
     */
    public LocalDateTime getUpdated() {
        return updated;
    }



    /**
     *
     * @param updated is to set Entity's lat time modified data in LocalDateTime format
     */
    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractEntity)) return false;
        AbstractEntity<?> that = (AbstractEntity<?>) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(version, that.version) &&

                Objects.equals(updated, that.updated);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, version, updated);
    }
}
