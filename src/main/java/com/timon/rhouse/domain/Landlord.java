package com.timon.rhouse.domain;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.javamoney.moneta.FastMoney;

import javax.money.Monetary;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity(name = Landlord.TBL_NAME)
public class Landlord extends AbstractClient {

    /**
     * TBL_NAME is table name of Landlord entities
     */
    public static final String TBL_NAME="landlords";

    /**
     * FLD_MONEY is name of column for #{@link Landlord#money}
     */
    public static final String FLD_MONEY="money";
    /**
     * FLD_MONEY is name of column for #{@link Landlord#contactNumber}
     */
    public static final String FLD_NUMBER="contactNumber";


    /**
     * Value name: flats
     * Type: List Flat - is list of flats of landlord
     * Is: list of flat of Landlord
     * OneToMany connection with Flat.class , One landlord can have many flats entities
     */
    @JsonManagedReference(value = "landlord")
    @OneToMany(mappedBy = Flat.FLD_LANDLORDS,orphanRemoval = true,fetch = FetchType.LAZY)
    List<Flat> flats;

    /**
     * Value name: money
     * Type: FastMoney object
     * Is: is money how much Landlord has
     * Can't be nullable
     */
    @Column(name = FLD_MONEY,nullable = false)
    FastMoney money;



    /**
     * Value name: contactNumber
     * Type: String
     * Is: is contact number of Landlord
     * Can be nullable and Unique
     * Size min:2 max:15
     */
    @Column(name = FLD_NUMBER,unique = true,nullable = true)
    @Size(min = 2,max =15,message = "Details can be 2-15")
    private String contactNumber;

    /**
     * Empty Constructor for Landlord
     */
    public Landlord() {
    }

    /**
     * Constructor for Landlord with 9 parameters , 6 of them from extended class AbstractClient
     * @param username is taken from super class. Username of Landlord.Type:String
     * @param name is taken from super class. Name of Landlord.Type:String
     * @param surname is taken from super class. Surname of Landlord.Type:String
     * @param email is taken from super class. Email of Landlord.Type:String
     * @param password is taken from super class. Password of Landlord.Type:String
     * @param roles is taken from super class. Roles of Landlord.Type:String
     * @param money is money in BigDecimal , constructor will create from BigDecimaal - FastMoney object
     * @param currencyUnit is currency for Money.Type:String
     * @param contactNumber is contact number of Landlord. Type:String
     */
    public Landlord(String username, String name,String surname, String email,String contactNumber, String password, BigDecimal money,String currencyUnit,String roles) {
        super(username, name,surname, email, password,roles);
        this.contactNumber=contactNumber;
        this.flats=new ArrayList<>();
        this.money=FastMoney.of(money, Monetary.getCurrency(currencyUnit));
    }

    /**
     * Getter for {@link Landlord#contactNumber}
     * @return String number of Landlord
     */
    public String getContactNumber() {
        return contactNumber;
    }

    /**
     * Setter for number of Landlord
     * @param contactNumber is String to change number of Landlord
     */
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    /**
     * Getter for {@link Landlord#flats}
     * @return list of flats of Landlord
     */
    public List<Flat> getFlats() {
        return flats;
    }

    /**
     * Setter for {@link Landlord#flats}
     * @param flats to set Flats of Landlord
     */
    public void setFlats(List<Flat> flats) {
        this.flats = flats;
    }

    /**
     * Getter for {@link Landlord#money}
     * @return FastMoney Object to get money amoun of Landlord
     */
    public FastMoney getMoney() {
        return money;
    }

    /**
     * Setter for {@link Landlord#money}
     * @param money is to set amount of money of Landlord with FastMoney object
     */
    public void setMoney(FastMoney money) {
        this.money = money;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Landlord)) return false;
        if (!super.equals(o)) return false;
        Landlord landlord = (Landlord) o;
        return Objects.equals(flats, landlord.flats) &&
                Objects.equals(money, landlord.money) &&
                contactNumber.equals(landlord.contactNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), flats, money, contactNumber);
    }
}
