package com.timon.rhouse.domain;

import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity(name=AbstractClient.TBL_NAME)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class AbstractClient extends AbstractEntity<Long> {

    /**
     * TBL_NAME is name of Table to store in database
     */
    public static final String TBL_NAME="clients";
    /**
     * FLD_NAME is name of column for {@link AbstractClient#name}  in database
     */
    public static final String FLD_NAME="name";
    /**
     * FLD_USERNAME is name of column for {@link AbstractClient#username} in database
     */
    public static final String FLD_USERNAME="username";
    /**
     * FLD_EMAIL is name of column for {@link AbstractClient#email}  in database
     */
    public static final String FLD_EMAIL="email";
    /**
     * FLD_PASSWORD is name of column for {@link AbstractClient#password}  in database
     */
    public static final String FLD_PASSWORD="password";
    /**
     * FLD_ROLES is name of column for {@link AbstractClient#roles}  in database
     */
    public static final String FLD_ROLES="roles";
    /**
     * FLD_SURNAME is name of column for {@link AbstractClient#surname}  in database
     */
    public static final String FLD_SURNAME="surname";


    /**
     * Value name: username
     * Type: String
     * Is: username of Client
     * Can't be null and unique
     * Size can be from 2 to 30
     */
    @Column(name = FLD_USERNAME,unique = true,nullable = false)
    @NotNull(message = "username can't be null")
    @Size(min = 2,max =30,message = "Length of username can be 2-30")
    private String username;

    /**
     * Value name: name
     * Type: String
     * Is: name of Client
     * Can't be null and NOT unique
     * Size can be from 2 to 30
     */
    @Column(name = FLD_NAME,unique = false,nullable = false)
    @NotNull(message = "name of Client cant  be null")
    @Size(min = 2,max =30,message = "Length of Client name  can be 2-30")
    private String name;


    /**
     * Value name: surname
     * Type: String
     * Is: surname of Client
     * Can't be null and NOT unique
     * Size can be from 2 to 30
     */
    @Column(name = FLD_SURNAME,unique = false,nullable = false)
    @NotNull(message = "surname of Client cant  be null")
    @Size(min = 2,max =30,message = "Length of Client surname  can be 2-30")
    private String surname;



    /**
     * Value name: email
     * Type: String
     * Is: email of Client
     * Can't be null and unique
     * Size can be from 2 to 30
     */
    @Column(name = FLD_EMAIL,unique = true,nullable = false)
    @NotNull(message = "email of Client cant  be null")
    @Size(min = 2,max =30,message = "Length of Client email  can be 2-30")
    private String email;

    /**
     * Value name: password
     * Type: String
     * Is: password of Client
     * Can't be null and NOT unique
     */
    @Column(name = FLD_PASSWORD,unique = false,nullable = false)
    @NotNull(message = "password of Client cant  be null")
    private String password;

    /**
     * Value name: roles
     * Type: String
     * Is: role of Client
     * Can't be null and NOT unique
     */
    @Column(name = FLD_ROLES,unique = false,nullable = false)
    private String roles;


    /**
     * Empty default Constructor
     */
    public AbstractClient(){

    }

    /**
     * Constructor for AbstractClient with 6 parameters
     * @param username is username of Client. Type:String
     * @param name is name of Client. Type:String
     * @param surname is surname of Client. Type:String
     * @param email is email of Client. Type:String
     * @param password is password of Client. Type:String. Password is encoding with Bcrypt.
     * @param roles is roles of Client. Type:String
     */
    public AbstractClient(String username,String name,String surname,String email, String password,String roles) {
        this.surname=surname;
        this.username = username;
        this.name = name;
        this.email = email;
        this.password = hashPassword(password);
        this.roles=roles;
    }


    /**
     * Getter for {@link AbstractClient#surname}
     * @return surname of Client
     * Type: String
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Setter for {@link AbstractClient#surname}
     * @param surname - setting surname of Client
     * Type: String
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Getter for {@link AbstractClient#roles}
     * @return roles of Client
     * Type: String
     */
    public String getRoles() {
        return roles;
    }

    /**
     * Setter for {@link AbstractClient#roles}
     * @param roles - setting roles of Client
     * Type: String
     */
    public void setRoles(String roles) {
        this.roles = roles;
    }

    /**
     * Getter for {@link AbstractClient#username}
     * @return usernaame of Client
     * Type: String
     */
    public String getUsername() {
        return username;
    }

    /**
     * Setter for {@link AbstractClient#username}
     * @param username - setting username of Client
     * Type: String
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Getter for {@link AbstractClient#name}
     * @return name of Client
     * Type: String
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for {@link AbstractClient#name}
     * @param name - setting naame of Client
     * Type: String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for {@link AbstractClient#email}
     * @return email of Client
     * Type: String
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter for {@link AbstractClient#email}
     * @param email - setting email of Client
     * Type: String
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter for {@link AbstractClient#password}
     * @return password of Client
     * Type: String
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter for {@link AbstractClient#password}
     * @param password - setting password of Client, using method hashPassword to encode password
     * Type: String
     */
    public void setPassword(String password) {
        this.password = hashPassword(password);
    }


    /**
     * Method hashPaassword used to hash password with Bcrypt
     * @param plainTextPassword - password in String format
     * @return hashed password
     */
    private String hashPassword(String plainTextPassword){
        return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractClient)) return false;
        if (!super.equals(o)) return false;
        AbstractClient that = (AbstractClient) o;
        return username.equals(that.username) &&
                name.equals(that.name) &&
                surname.equals(that.surname) &&
                email.equals(that.email) &&
                password.equals(that.password)&&
                roles.equals(that.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), username, name, surname, email, password,roles);
    }
}
