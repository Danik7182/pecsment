package com.timon.rhouse.controller;

import com.sun.mail.iap.Response;
import com.timon.rhouse.domain.Consumer;
import com.timon.rhouse.exception.ResourceNotFoundException;
import com.timon.rhouse.repository.ConsumerRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;


import javax.swing.text.html.HTML;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/consumer")
public class ConsumerAPI {

    /**
     * Logger from log4j
     */
    private final static Logger LOG = LogManager.getLogger();

    @Autowired
    ConsumerRepository consumerRepository;


    /**
     * REST API endpoint - /consumer/create
     * Method: POST
     * Method createConsumer is used to create and save new Consumer
     * Method requires one Request Body (parameter)
     * @param consumer is Consumer object
     * @return HttpStatus if created well - 200 if not status 500
     */
    @PostMapping("/create")
    public ResponseEntity<HttpStatus> createConsumer(@RequestBody Consumer consumer)  {
        try{
            consumerRepository.save(consumer);
            return new ResponseEntity<>(HttpStatus.OK);

        }catch (Exception e){
            LOG.error(e + " data: " + consumer);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    /**
     * REST API endpoint - /consumer/update/id
     * Method: PUT
     * Method updateConsumer is used to update and save old Consumer
     * Method requires one PathVariable long id, id of Consumer
     * @param id is id of Consumer which has to be update
     * @param _consumer is Consumer with another data
     * @return HttpStatus if updated well - 200 code if not status 500
     */
    @PutMapping("/update/{id}")
    public ResponseEntity<Consumer> updateConsumer(@PathVariable("id") long id,@RequestBody Consumer _consumer){

        try{
            Consumer consumer =consumerRepository.getOne(id);
            if(consumer==null){
                throw new ResourceNotFoundException("Can't find consumer with id: " + id);
            }
            consumer.setEmail(_consumer.getEmail());
            consumer.setName(_consumer.getName());
            consumer.setSurname(_consumer.getSurname());
            consumer.setPassword(_consumer.getPassword());
            consumer.setUsername(_consumer.getUsername());

            return new ResponseEntity<>(consumerRepository.save(consumer),HttpStatus.OK);
        }catch (Exception e){

            LOG.error(e + " data " + _consumer );
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * REST API endpoint - /consumer/xr
     * Method: GET
     * Method getAllConsumer is to get All Consumer from database
     * @return HttpStatus if found  - 200 code and list of Consumer if not status 500 and null
     */
    @GetMapping("/getAll")
   public ResponseEntity<List<Consumer>> getAllConsumer(){

       try{
           List<Consumer> consumers = consumerRepository.findAll();

           if(consumers.isEmpty()){
               LOG.error("Consumer list is empty");
               return new ResponseEntity<>(consumers,HttpStatus.NO_CONTENT);
           }
           return new ResponseEntity<>(consumers,HttpStatus.OK);

       }catch (Exception e){
           LOG.error(  e);
           return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
       }
   }

    /**
     * REST API endpoint - /consumer/getById/id
     * Method: GET
     * Method getConsumerById is used to get one Consumer from parameter id
     * Method required one PathVariable long id , id of Consumer
     * @param id is Consumer's id
     * @return HttpStatus if found  - 200 code and Consumer object if not throws ResourceNotFoundException with message:"No Consumer found  with id - " + and id of Consumer
     */
   @GetMapping("/getById/{id}")
   public ResponseEntity<Consumer> getConsumerById(@PathVariable("id")  long id){

            Consumer consumer = consumerRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("No Consumer found  with id - " + id));
            return new ResponseEntity<>(consumer, HttpStatus.OK);

   }

    /**
     * REST API endpoint - /consumer/getByEmail/email
     * Method: GET
     * Method getConsumerByEmail is used find and return Consumer by email
     * Method required one Path Variable String email, email of Consumer
     * @param email is Consumer's email
     * @return HttpStatus if found  - 200 code and Consumer Object if not throws ResourceNotFoundException with message:"No Consumer found  with email - " + and email of Consumer
     */
   @GetMapping("/getByEmail/{email}")
    public ResponseEntity<Consumer> getConsumerByEmail(@PathVariable("email") String email){

       Consumer consumer = consumerRepository.findByEmail(email);
       if(consumer==null){
           LOG.info("Consumer with email: " + email +  " not found ");
           throw new ResourceNotFoundException("No Consumer found  with Email - " + email);
       }
       return new ResponseEntity<>(consumer, HttpStatus.OK);
   }

    /**
     * REST API endpoint - /consumer/getByUsername/username
     * Method: GET
     * Method getConsumerByUsername is used find and return Consumer by username
     * Method required one Path Variable String username, username of Consumer
     * @param username is Consumer object
     * @return HttpStatus if found  - 200 code and Consumer object if not throws ResourceNotFoundException with message:"No Consumer found  with Username - " + and username of Consumer
     */
    @GetMapping("/getByUsername/{username}")
    public ResponseEntity<Consumer> getConsumerByUsername(@PathVariable("username") String username){

        Consumer consumer = consumerRepository.findByUsername(username);
        if(consumer==null){
            LOG.info("Consumer with username: " + username +  " not found ");
            throw new ResourceNotFoundException("No Consumer found  with Username - " + username);
        }
        return new ResponseEntity<>(consumer, HttpStatus.OK);
    }

    /**
     * REST API endpoint - /consumer/getByName/name
     * Method: GET
     * Method getAllConsumerByName is used find and return  all Consumers with this name(paramter)
     * Method required one Path Variable String name, name of Consumers
     * @param name is String name of Consumers
     * @return HttpStatus if found  - 200 code and List of Consumers if not throws ResourceNotFoundException with message:"No Consumer found  with name - " + and name of Consumer
     */
    @GetMapping("/getByName/{name}")
    public ResponseEntity<List<Consumer>> getAllConsumerByName(@PathVariable("name") String name){

        List<Consumer> consumers = consumerRepository.findAllByName(name);
        if(consumers.isEmpty()){
            LOG.info("Consumers with name: " + name +  " not found ");
            throw new ResourceNotFoundException("No Consumer found  with Name - " + name);
        }
        return new ResponseEntity<>(consumers, HttpStatus.OK);
    }

    /**
     * REST API endpoint - /consumer/getBySurname/name
     * Method: GET
     * Method getAllConsumerByName is used find and return  all Consumers with this surname(paramter)
     * Method required one Path Variable String surname, surname of Consumers
     * @param surname is String surname of Consumers
     * @return HttpStatus if found  - 200 code and List of Consumers if not throws ResourceNotFoundException with message:"No Consumer found  with surname - " + and surname of Consumer
     */
    @GetMapping("/getBySurname/{surname}")
    public ResponseEntity<List<Consumer>> getAllConsumerBySurname(@PathVariable("surname") String surname){

        List<Consumer> consumers = consumerRepository.findAllBySurname(surname);
        if(consumers.isEmpty()){
            LOG.info("Consumers with name: " + surname +  " not found ");
            throw new ResourceNotFoundException("No Consumer found  with surname - " + surname);
        }
        return new ResponseEntity<>(consumers, HttpStatus.OK);
    }

    /**
     * REST API endpoint - /consumer/getByNameAndSurname/name/surname
     * Method: GET
     * Method getAllConsumerByNameAndSurname is used find and return  all Consumers with this name(paraameter) and surname(paramter)
     * Method required two Path Variables String surname,String name; name , surname of Consumers
     * @param surname is String surname of Consumers
     * @param name is String name of Consumers
     * @return HttpStatus if found  - 200 code and List of Consumers if not throws ResourceNotFoundException with message:"Consumers with name and surname: " + name + " " +surname +  " not found "
     */
    @GetMapping("/getByNameAndSurname/{name}/{surname}")
    public ResponseEntity<List<Consumer>> getAllConsumerByNameAndSurname(@PathVariable("name") String name,@PathVariable("surname") String surname){
        List<Consumer> consumers = consumerRepository.findAllByNameAndSurname(name,surname);
        if(consumers.isEmpty()){
            LOG.info("Consumers with name and surname: " + name + " " +surname +  " not found ");
            throw new ResourceNotFoundException("Consumers with name and surname: " + name + " " +surname +  " not found ");
        }
        return new ResponseEntity<>(consumers, HttpStatus.OK);
    }

    /**
     * REST API endpoint - /consumer/deleteAll
     * Method: DELETE
     * Method deleteAllConsumers is used to delete all Consumers
     * @return HttpStatus if deleted well - 204 if not status 500
     * @throws Exception - will not throw exception just sens Http Status - INTERNAL_SERVER_ERROR
     */

    @DeleteMapping("/deleteAll")
    public ResponseEntity<HttpStatus> deleteAllConsumers() throws Exception {
        try {
            consumerRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            LOG.error(e + " Can't delete all consumers ");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);


        }
    }

    /**
     * REST API endpoint - /consumer/deleteById/id
     * Method: DELETE
     * Method deleteConsumerById is used to delete Consumer by id
     * Method required one PathVariable long id (parameter)
     * @param id is Consumer's id which has to be deleted
     * @return HttpStatus if deleted well - 204 if not status 500
     * @throws Exception if id is less than 0
     */
    @DeleteMapping("/deleteById/{id}")
    public ResponseEntity<HttpStatus> deleteConsumerById(@PathVariable("id") long id) throws Exception {

        if( id<=0) {
            LOG.error("Id <= 0 : " + id);
            throw new Exception("Id == 0");
        }
        try{
            consumerRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            LOG.error(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    /**
     * REST API endpoint - /consumer/deleteByUsername/username
     * Method: DELETE
     * Method deleteConsumerByUsername is used to delete Consumer by username
     * Method required one PathVariable long id (parameter)
     * @param username is Consumer's username which has to be deleted
     * @return HttpStatus if deleted well - 204 if not status 500
     * @throws Exception if username is null
     */
    @DeleteMapping("/deleteByUsername/{username}")
    public ResponseEntity<HttpStatus> deleteConsumerByUsername(@PathVariable("username") String username) throws Exception {

        if( username==null) {
            LOG.error("username ==null " + username);
            throw new Exception("username == null");
        }
        try{
            if(consumerRepository.findByUsername(username)==null){
                throw new ResourceNotFoundException("No Consumer found  with Username - " + username);
            }
            consumerRepository.deleteByUsername(username);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            LOG.error(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * REST API endpoint - /consumer/deleteByEmail/email
     * Method: DELETE
     * Method deleteConsumerById is used to delete Consumer by email
     * Method required one PathVariable long id (parameter)
     * @param email is Consumer's email which has to be deleted
     * @return HttpStatus if deleted well - 204 if not status 500
     * @throws Exception if email is null
     */

    @DeleteMapping("/deleteByEmail/{email}")
    public ResponseEntity<HttpStatus> deleteConsumerByEmail(@PathVariable("email") String email) throws Exception {

        if( email==null) {
            LOG.error("email ==null " + email);
            throw new Exception("email == null");
        }
        try{
            if(consumerRepository.findByEmail(email)==null){
                LOG.info("Consumer with email: " + email +  " not found ");
                throw new ResourceNotFoundException("No Consumer found  with Email - " + email);

            }
            consumerRepository.deleteByEmail(email);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            LOG.error(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * REST API endpoint - /consumer/deleteAllByName/name
     * Method: DELETE
     * Method deleteAllConsumerByName is used to delete all  Consumers with this name(parameter)
     * Method required one PathVariable String name (parameter)
     * @param name is Consumer's name which has to be deleted
     * @return HttpStatus if deleted well - 204 if not status 500
     * @throws Exception if name is null
     */
    @DeleteMapping("/deleteAllByName/{name}")
    public ResponseEntity<HttpStatus> deleteAllConsumerByName(@PathVariable("name") String name) throws Exception {

        if( name==null) {
            LOG.error("name ==null " + name);
            throw new Exception("name == null");
        }
        try{
            if(consumerRepository.findAllByName(name).isEmpty()){
                LOG.info("Consumers with name: " + name +  " not found ");
                throw new ResourceNotFoundException("No Consumer found  with Name - " + name);
            }
            consumerRepository.deleteAllByName(name);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            LOG.error(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * REST API endpoint - /consumer/deleteAllBySurname/surname
     * Method: DELETE
     * Method deleteAllBySurname is used to delete all  Consumers with this surname(parameter)
     * Method required one PathVariable String surname (parameter)
     * @param surname is Consumer's surname which has to be deleted
     * @return HttpStatus if deleted well - 204 if not status 500
     * @throws Exception if surname is null
     */
    @DeleteMapping("/deleteAllBySurname/{surname}")
    public ResponseEntity<HttpStatus> deleteAllConsumerBySurname(@PathVariable("surname") String surname) throws Exception {

        if( surname==null) {
            LOG.error("surname ==null " + surname);
            throw new Exception("surname == null");
        }
        try{
            if(consumerRepository.findAllBySurname(surname).isEmpty()){
                LOG.info("Consumers with surname: " + surname +  " not found ");
                throw new ResourceNotFoundException("No Consumer found  with surname - " + surname);
            }
            consumerRepository.deleteAllBySurname(surname);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            LOG.error(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * REST API endpoint - /consumer/deleteAllByNameAndSurname/naame/surname
     * Method: DELETE
     * Method deleteAllBySurname is used to delete all  Consumers with this name(parameter) and  surname(parameter)
     * Method required one PathVariable String surname (parameter)
     * @param surname is Consumer's surname
     * @param name is Consumer's name
     * @return HttpStatus if deleted well - 204 if not status 500
     * @throws Exception if surname or name is null
     */
    @DeleteMapping("/deleteAllByNameAndSurname/{name}/{surname}")
    public ResponseEntity<HttpStatus> deleteAllConsumerByNameAndSurname(@PathVariable("name") String name,@PathVariable("surname") String surname) throws Exception {

        if( surname==null||name==null) {
            LOG.error("surname == null || name ==null  " + surname + " " + name);
            throw new Exception("surname == null || name ==null ");
        }
        try{
            if(consumerRepository.findAllByNameAndSurname(name,surname).isEmpty()){
                LOG.info("Consumers with name and surname : " + name + " " + surname+  " not found ");
                throw new ResourceNotFoundException("Consumers with name and surname : " + name + " " + surname+  " not found ");
            }
            consumerRepository.deleteAllByNameAndSurname(name,surname);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            LOG.error(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
