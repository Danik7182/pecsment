package com.timon.rhouse.controller;



import com.timon.rhouse.controller.specification.FlatSpec;
import com.timon.rhouse.domain.Flat;
import com.timon.rhouse.domain.Landlord;
import com.timon.rhouse.exception.ResourceNotFoundException;
import com.timon.rhouse.repository.FlatRepository;
import com.timon.rhouse.repository.LandlordRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javamoney.moneta.FastMoney;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.money.Monetary;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;



@RestController
@RequestMapping("/flat")
public class FlatAPI {

    @Autowired
    FlatRepository flatRepository;

    @Autowired
    LandlordRepository landlordRepository;
    private final static Logger LOG = LogManager.getLogger();


    /**
     * REST API endpoint - /flat/create
     * Method: POST
     * Method createFlat is used to create and save new Flat
     * Method requires one Request Body- Flat(parameter)
     * @param flat is Flat object
     * @return HttpStatus if created well - 200 if not status 500
     */
    @PostMapping("/create")
    public ResponseEntity<HttpStatus> createFlat(@RequestBody Flat flat)  {

        try{
            flatRepository.save(flat);
            return new ResponseEntity<>(HttpStatus.OK);

        }catch (Exception e){
            LOG.error(e + " data: " + flat);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * REST API endpoint - /flat/getAll
     * Method: GET
     * Method getAllFlat is used to get List of all Flats
     * @return HttpStatus if created well - 200 if not throws ResourceNotFounException
     */
    @GetMapping("/getAll")
    public ResponseEntity<List<Flat>> getAllFlat(){
        return new ResponseEntity<>(flatRepository.findAll(),HttpStatus.OK);
    }

    /**
     * REST API endpoint - /flat/getById/id
     * Method: GET
     * Method getFlatById is used to get one Flat from parameter id
     * Method required one PathVariable long id , id of Flat
     * @param id is Flat's id
     * @return HttpStatus if found  - 200 code and Consumer object if not throws ResourceNotFoundException with message:No Flat found  with id - " + id
     */
    @GetMapping("/getById/{id}")
    public ResponseEntity<Flat> getFlatById(@PathVariable("id") long id){

        Flat flat = flatRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("No Flat found  with id - " + id));
        return new ResponseEntity<>(flat, HttpStatus.OK);

    }



    /**
     * REST API endpoint - /flat/advancedSearch
     * Method: GET
     * Method getAllFlatAdvanced is used to get Flat with many parameters
     * All parameters are NOT required
     * @param cityname - (String) - city name of FLat
     * @param priceLess - (BigDecimal) - minimum Price
     * @param priceGreater - (BigDecimal) - maximum Price
     * @param untilityLess - (BigDecimal) - minimum Utilities
     * @param untilityGreater - (BigDecimal) - maximum Utilities
     * @param rentORbuy - (Boolean) - Renting Flat(true) or Selling Flat(false)
     * @param furnished - (Boolean) - Furnished Flat or not
     * @param renovated - (Boolean) - Renovated Flat or not
     * @param roomsLess - (Integer) - minimum number of rooms
     * @param roomsGreater - (Integer) - maximum number of rooms
     * @param wifi - (Boolean) - wifi is exist or not
     * @param internetSpeed - (Float) - speed of internet
     * @param availableDate - (String) - data available in format - year.mm.dd
     * @return will return List of Flats and Status is 200
     */
    @GetMapping("/advancedSearch")
    public ResponseEntity<List<Flat>> getAllFlatAdvanced(@RequestParam(required = false) String cityname,
                                               @RequestParam(required = false) BigDecimal priceLess,
                                               @RequestParam(required = false) BigDecimal priceGreater,
                                               @RequestParam(required = false) BigDecimal untilityLess,
                                               @RequestParam(required = false) BigDecimal untilityGreater,
                                               @RequestParam(required = false) Boolean rentORbuy,
                                               @RequestParam(required = false) Boolean furnished,
                                               @RequestParam(required = false) Boolean renovated,
                                               @RequestParam(required = false) Integer roomsLess,
                                               @RequestParam(required = false) Integer roomsGreater,
                                               @RequestParam(required = false) Boolean wifi,
                                               @RequestParam(required = false) Float internetSpeed,
                                               @RequestParam(required = false) String availableDate
                                               ){

        FastMoney priceLess1=null;
        FastMoney priceGreater1=null;
        FastMoney untilityLess1=null;
        FastMoney untilityGreater1=null;

        if(priceGreater!=null && priceLess!=null){
            priceLess1=FastMoney.of(priceLess, Monetary.getCurrency("EUR"));
            priceGreater1=FastMoney.of(priceGreater, Monetary.getCurrency("EUR"));
        }

        if(untilityGreater!=null && untilityLess!=null){
            untilityLess1=FastMoney.of(untilityLess, Monetary.getCurrency("EUR"));
            untilityGreater1=FastMoney.of(untilityGreater, Monetary.getCurrency("EUR"));


        }

        LocalDate availableFrom=null;

        if(availableDate!=null){
            String[] availables=availableDate.split("-");
            availableFrom=LocalDate.of(Integer.parseInt(availables[0]),Integer.parseInt(availables[1]),Integer.parseInt(availables[2]));
        }


        Specification spec1 = FlatSpec.cityNameEquals(cityname);
        Specification spec2 = FlatSpec.priceBetween(priceLess1,priceGreater1);
        Specification spec3 = FlatSpec.utilitiesBetween(untilityLess1,untilityGreater1);
        Specification spec4 = FlatSpec.rentOrBuy(rentORbuy);
        Specification spec5 = FlatSpec.isRenovated(renovated);
        Specification spec6 = FlatSpec.isFurnished(furnished);
        Specification spec7 = FlatSpec.roomsBetween(roomsLess,roomsGreater);
        Specification spec8 = FlatSpec.isWifi(wifi);
        Specification spec9 = FlatSpec.wifiSpeed(internetSpeed);
        Specification spec10 = FlatSpec.availableFrom(availableFrom);

        Specification spec = Specification.where(spec1).and(spec2).and(spec3)
                .and(spec4).and(spec5).and(spec6).and(spec7).and(spec8).and(spec9).and(spec10);


        return new ResponseEntity<>(flatRepository.findAll(spec),HttpStatus.OK);


    }


    /**
     * REST API endpoint - /flat/deleteAll
     * Method: DELETE
     * Method deleteAllFlat is used to delete all Flats
     * @return HttpStatus if deleted well - 204 if not status 500
     */
    @DeleteMapping("/deleteAll")
    public ResponseEntity<HttpStatus> deleteAllFlat() {

        try{
            flatRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        }catch (Exception e){
            LOG.error(e +" Can't DELETE ALL");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }


    }

    /**
     * REST API endpoint - /flat/deleteById/id
     * Method: DELETE
     * Method deleteFlatById is used to delete Flat by id
     * Method required one PathVariable long id (parameter)
     * @param id is Flat's id which has to be deleted
     * @return HttpStatus if deleted well - 204 if not status 500
     * @throws Exception if id is null
     */
    @DeleteMapping("/deleteById/{id}")
    public ResponseEntity<HttpStatus> deleteFlatById(@PathVariable("id") Long id)throws Exception{
        if(id==null){
            throw new Exception("ID is NUll " +id);
        }
        try{
            flatRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            LOG.error(e);
            throw new Exception(e +  " " + id);
        }

    }



    /**
     * REST API endpoint - /flat/update/id
     * Method: PUT
     * Method updateConsumer is used to update and save old Consumer
     * Method requires one PathVariable long id, id of Flat to be updated
     * @param id is id of Flat which has to be update
     * @param _flat is Flat object with another data filled
     * @return HttpStatus if updated well - 200 code if not status 500
     * @throws Exception if id is null and if can't update flat
     */
    @PutMapping("/update/{id}")
    public ResponseEntity<Flat> updateFlatData(@RequestBody Flat _flat,@RequestParam Long id) throws Exception{

        if(id==null){
            throw new Exception("Id is null");
        }
        try{
            Flat flat = flatRepository.findById(id)
                    .orElseThrow(()->new ResourceNotFoundException("Can't find flat with id " + id ));

          
            flat.setInternetSpeed(_flat.getInternetSpeed());
            flat.setAvailableFrom(_flat.getAvailableFrom());
            flat.setNumberOfBeds(_flat.getNumberOfBeds());
            flat.setNumberOfRooms(_flat.getNumberOfRooms());
            flat.setWifi(_flat.isWifi());
            flat.setRenovated(_flat.isRenovated());
            flat.setFurnished(_flat.isFurnished());
            flat.setTitle(_flat.getTitle());
            flat.setCity(_flat.getCity());
            flat.setAddress(_flat.getAddress());
            flat.setPrice(_flat.getPrice());
            flat.setUtilities(_flat.getUtilities());
            flat.setTotal(flat.getPrice().add(flat.getUtilities()));
            flat.setDetails(_flat.getDetails());
            flat.setRentORbuy(_flat.getRentORbuy());
            flat.setLandlord(_flat.getLandlord());

            flatRepository.save(flat);

            return new ResponseEntity<>(flat,HttpStatus.OK);

        }catch (Exception e){
            LOG.error("Can't update flat " + e);
            throw new Exception(e);
        }
    }
}
