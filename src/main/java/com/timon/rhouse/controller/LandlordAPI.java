package com.timon.rhouse.controller;

import com.timon.rhouse.domain.Consumer;
import com.timon.rhouse.domain.Landlord;
import com.timon.rhouse.exception.ResourceNotFoundException;
import com.timon.rhouse.repository.LandlordRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javamoney.moneta.FastMoney;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.money.Monetary;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/landlord")
public class LandlordAPI {


    @Autowired
    LandlordRepository landlordRepository;

    /**
     * Logger from log4j
     */
    private final static Logger LOG = LogManager.getLogger();


    Pageable firstPageWithFive = PageRequest.of(0, 5);


    /**
     * REST API endpoint - /landlord/create
     * Method: POST
     * Method createLandlord is used to create and save new Landlord
     * Method requires one Request Body (parameter)
     * @param landlord is Landlord object
     * @return HttpStatus if created well - 200 if not status 500
     * @throws Exception if landlord is null
     */
    @PostMapping("/create")
    public ResponseEntity<Landlord> createLandlord(@RequestBody Landlord landlord) throws Exception{

        if(landlord==null){
            throw new Exception("Lanlord is null + " + landlord);
        }
        try{
            landlordRepository.save(landlord);
            return new ResponseEntity<>(landlord,HttpStatus.OK);
        }catch (Exception e){
            LOG.error(e + " Can't create new entity Landlord + " + landlord);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * REST API endpoint - /landlord/getAll
     * Method: GET
     * Method getAllLandlords is used to get All Landlords
     * @return HttpStatus if found  well - 200 code and list of Landlords  if not status 500
     * @throws Exception if landlord is null
     */
    @GetMapping("/getAll")
    public ResponseEntity<Page<Landlord>> getAllLandlords()throws Exception{
        try{
            Page<Landlord> landlords=landlordRepository.findAll(firstPageWithFive);
            if(landlords.isEmpty()){
                throw new Exception("List of landlords is empty");
            }
            return new ResponseEntity<>(landlords,HttpStatus.OK);
        }catch (Exception e){
            LOG.error(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * REST API endpoint - /landlord/getById/id
     * Method: GET
     * Method getLandlordById is used to get one Landlord from parameter id
     * Method required one PathVariable long id , id of Landlord
     * @param id is Landlords's id
     * @return HttpStatus if found  - 200 code and Landlord object if not throws ResourceNotFoundException with message:Can't find lanlord with id - " + id
     * @throws Exception if id is null
     */
    @GetMapping("/getById/{id}")
    public  ResponseEntity<Landlord> getLandlordById(@PathVariable("id") Long id) throws Exception {
        if(id==null){
            LOG.error("Null id given "+ id);
            throw new Exception("Id is null");
        }
        try{

            Landlord landlord=landlordRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Can't find lanlord with id - " + id));
            return new ResponseEntity<Landlord>(landlord,HttpStatus.OK);

        }catch (Exception e){
            LOG.error(e + " id " +id);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


    /**
     * REST API endpoint - /landlord/getByEmail/email
     * Method: GET
     * Method getConsumerByEmail is used find and return Landlord by email
     * Method required one Path Variable String email, email of Landlord
     * @param email is Landlord's email
     * @return HttpStatus if found  - 200 code and Landlord Object if not throws ResourceNotFoundException with message:"Can't find landlord with email - "+ email
     * @throws Exception if emaiall is null
     */
    @GetMapping("/getByEmail/{email}")
    public ResponseEntity<Landlord> getLandlordByEmail(@PathVariable("email") String email) throws Exception{
        if(email==null){
            LOG.error("email is null");
            throw new Exception("Email is null");
        }
        try{
            Landlord landlord =landlordRepository.findByEmail(email);
            if(landlord==null){
                throw new ResourceNotFoundException("Can't find landlord with email - "+ email);
            }
            return new ResponseEntity<>(landlord,HttpStatus.OK);
        }catch (Exception e){
            LOG.error(e+"email- "+ email);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * REST API endpoint - /landlord/getByUsername/username
     * Method: GET
     * Method getLandlordByUsername is used find and return Landlord by username
     * Method required one Path Variable String username, username of Landlord
     * @param username is Landlord object
     * @return HttpStatus if found  - 200 code and Landlord object if not throws ResourceNotFoundException with message:"Can't find landlord with username - "+ username
     * @throws Exception if username is null
     */
    @GetMapping("/getByUsername/{username}")
    public ResponseEntity<Landlord> getLandlordByUsername(@PathVariable("username") String username) throws Exception{
        if(username==null){
            LOG.error("username is null");
            throw new Exception("username is null");
        }
        try{
            Landlord landlord =landlordRepository.findByUsername(username);
            if(landlord==null){
                throw new ResourceNotFoundException("Can't find landlord with username - "+ username);
            }
            return new ResponseEntity<>(landlord,HttpStatus.OK);
        }catch (Exception e){
            LOG.error(e+"username- "+ username);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * REST API endpoint - /landlord/getAllByName/name
     * Method: GET
     * Method getLandlordByName is used find and return  all Landlords with this name(paramter)
     * Method required one Path Variable String name, name of Landlords
     * @param name is String name of Landlords
     * @return HttpStatus if found  - 200 code and List of Landlords if not throws ResourceNotFoundException with message:"Can't find landlords with name - "+ name
     * @throws Exception if name is null
     */
    @GetMapping("/getAllByName/{name}")
    public ResponseEntity<List<Landlord>> getLandlordByName(@PathVariable("name") String name) throws Exception{
        if(name==null){
            LOG.error("name is null");
            throw new Exception("name is null");
        }
        try{
            List<Landlord> landlord =landlordRepository.findAllByName(name);
            if(landlord.isEmpty()){
                throw new ResourceNotFoundException("Can't find landlords with name - "+ name);
            }
            return new ResponseEntity<>(landlord,HttpStatus.OK);
        }catch (Exception e){
            LOG.error(e+"name- "+ name);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * REST API endpoint - /landlord/deleteAll
     * Method: DELETE
     * Method deleteAllLandlords is used to delete all Laandlords
     * @return HttpStatus if deleted well - 204 if not status 500
     */
    @DeleteMapping("/deleteAll")
    public ResponseEntity<HttpStatus> deleteAllLandlords(){

        try{
            landlordRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            LOG.error("Can't delete- " + e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * REST API endpoint - /landlord/deleteById/id
     * Method: DELETE
     * Method deleteLandlordById is used to delete Landlord by id
     * Method required one PathVariable long id (parameter)
     * @param id is Landlord's id which has to be deleted
     * @return HttpStatus if deleted well - 204 if not status 500
     * @throws Exception if id is null
     */
    @DeleteMapping("/deleteById/{id}")
    public ResponseEntity<HttpStatus> deleteLandlordById(@PathVariable("id" )Long id)throws Exception{

        if(id==null){
            LOG.error("Id is null");
            throw new Exception("Id is null");
        }
        try{
            landlordRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            LOG.error("Can't delete lanlord with id - "+id+" " + e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * REST API endpoint - /landlord/deleteByEmail/email
     * Method: DELETE
     * Method deleteLanlordByEmail is used to delete Landlord by email
     * Method required one PathVariable long id (parameter)
     * @param email is Landlord's email which has to be deleted
     * @return HttpStatus if deleted well - 204 if not status 500
     * @throws Exception if email is null
     */
    @DeleteMapping("/deleteByEmail/{email}")
    public ResponseEntity<HttpStatus> deleteLanlordByEmail(@PathVariable("email" )String email)throws Exception{

        if(email==null){
            LOG.error("email is null");
            throw new Exception("email is null");
        }
        try{
            landlordRepository.deleteByEmail(email);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            LOG.error("Can't delete lanlord with email - "+email+" " + e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * REST API endpoint - /landlord/update/id
     * Method: PUT
     * Method updateLandlord is used to update and save old Landlord
     * Method requires one PathVariable long id, id of Landlord
     * @param id is id of Landlord which has to be update
     * @param _landlord is Landlord object with another data
     * @return HttpStatus if updated well - 200 code if not status 500
     */
    @PutMapping("/update/{id}")
    public ResponseEntity<Landlord> updateLandlord(@PathVariable("id") long id, @RequestBody Landlord _landlord){

        try{
            Landlord landlord =landlordRepository.getOne(id);
            if(landlord==null){
                LOG.error("Can't find consumer with id: " + id);
                throw new ResourceNotFoundException("Can't find consumer with id: " + id);
            }

            landlord.setUsername(_landlord.getUsername());
            landlord.setName(_landlord.getName());
            landlord.setSurname(_landlord.getSurname());
            landlord.setPassword(_landlord.getPassword());
            landlord.setUsername(_landlord.getUsername());
            landlord.setContactNumber(_landlord.getContactNumber());
            landlord.setFlats(_landlord.getFlats());

            return new ResponseEntity<>(landlordRepository.save(landlord),HttpStatus.OK);
        }catch (Exception e){

            LOG.error(e + " data " + _landlord );
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    /**
     * REST API endpoint - /landlord/popupBalance
     * Method: PUT
     * Method updateLandlord is used to add money to the account
     * Method requires 2 Request parameters BigDecimal moneyToAdd, id of Landlord
     * @param id is id of Landlord which has to be update
     * @param moneyToAdd is money to popup
     * @return HttpStatus if added well - 200 code if not status 500
     */
    @PutMapping("/popupBalance")
    public ResponseEntity<Landlord> popUpBalanceOfLanlord(@RequestParam BigDecimal moneyToAdd,@RequestParam Long id){

        try{
            Landlord landlord= landlordRepository.findById(id).get();
            if(landlord==null){
                throw new ResourceNotFoundException("Can't find landlord with id - "+ id);
            }
            FastMoney fastMoney=FastMoney.of(moneyToAdd, Monetary.getCurrency("EUR"));
            landlord.setMoney(landlord.getMoney().add(fastMoney));

            landlordRepository.save(landlord);
            return new ResponseEntity<>(landlord,HttpStatus.OK);
        }catch (Exception e){
            LOG.error("Can't add money for lanlord with id " + id +" " + e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * REST API endpoint - /landlord/subtractBalance
     * Method: PUT
     * Method substractBalanceOfLanlord is used to substract money to the account
     * Method requires 2 Request parameters BigDecimal moneyToSubtract, id of Landlord
     * @param id is id of Landlord which has to be update
     * @param moneyToSubtract is money to substract from balance
     * @return HttpStatus if substract well - 200 code if not status 500
     */
    @PutMapping("/subtractBalance")
    public ResponseEntity<Landlord> substractBalanceOfLanlord(@RequestParam BigDecimal moneyToSubtract,@RequestParam Long id) {

        try{
            Landlord landlord= landlordRepository.findById(id).get();
            if(landlord==null){
                throw new ResourceNotFoundException("Can't find landlord with id - "+ id);
            }
            FastMoney fastMoney=FastMoney.of(moneyToSubtract, Monetary.getCurrency("EUR"));
            landlord.setMoney(landlord.getMoney().subtract(fastMoney));

            landlordRepository.save(landlord);
            return new ResponseEntity<>(landlord,HttpStatus.OK);
        }catch (Exception e){
            LOG.error("Can't moneyToSubtract money for lanlord with id " + id +" " + e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
