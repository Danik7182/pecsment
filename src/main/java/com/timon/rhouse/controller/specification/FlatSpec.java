package com.timon.rhouse.controller.specification;

import com.timon.rhouse.domain.Flat;
import org.javamoney.moneta.FastMoney;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;

/**
 * FlatSpec class is used to search flats with different properties
 * Specification used to pass many arguments into query
 */
public class FlatSpec {


    /**
     * Method cityNameEquals is used to search flat by  city name
     * @param city String is name of City
     * @return Object
     */
    public static Specification<Flat> cityNameEquals(String city) {
        return (root, query, builder) ->
                city == null ?
                        builder.conjunction() :
                        builder.equal(root.get("city"), city);
    }

    /**
     * Method addressFlatEquals is used to search flat by address
     * @param address String is address
     * @return Object
     */
    public static Specification<Flat> addressFlatEquals(String address) {
        return (root, query, builder) ->
                address == null ?
                        builder.conjunction() :
                        builder.equal(root.get("address"), address);
    }

    /**
     * Method priceBetween is used to search flat price between 2 amounts
     * @param less is minimum price of flat
     * @param greater is maximum price of flat
     * @return Object
     */
    public static Specification<Flat> priceBetween(FastMoney less ,FastMoney greater) {
        return (root, query, builder) ->
                less == null ?
                        builder.conjunction() :
                        builder.between(root.get("price"), less,greater);
    }
    /**
     * Method utilitiesBetween is used to search flat utilities between 2  amounts
     * @param less is minimum utilities of flat
     * @param greater is maximum utilities of flat
     * @return Object
     */
    public static Specification<Flat> utilitiesBetween(FastMoney less ,FastMoney greater) {
        return (root, query, builder) ->
                less == null && greater==null ?
                        builder.conjunction() :
                        builder.between(root.get("utilities"), less,greater);
    }

    /**
     * Method rentOrBuy is used to search flat for renting or buying
     * @param rentOrBuy Boolean flat for rent or not,(true - rent, false-buy)
     * @return Object
     */
    public static Specification<Flat> rentOrBuy(Boolean rentOrBuy) {
        return (root, query, builder) ->
                rentOrBuy == null ?
                        builder.conjunction() :
                        builder.equal(root.get("rentORbuy"), rentOrBuy);
    }

    /**
     * Method roomsBetween is used to search flat between room numbers
     * @param less is minimum number of room of flat
     * @param greater is maximum number of room of flat
     * @return Object
     */
    public static Specification<Flat> roomsBetween(Integer less ,Integer greater) {
        return (root, query, builder) ->
                less == null && greater ==null ?
                        builder.conjunction() :
                        builder.between(root.get("numberOfRooms"), less,greater);
    }

    /**
     * Method isFurnished is used to search flat by furnished flat or not
     * @param furnished Boolean is flat furnished(true) or not(false)
     * @return Object
     */
    public static Specification<Flat> isFurnished(Boolean furnished) {
        return (root, query, builder) ->
                furnished == null ?
                        builder.conjunction() :
                        builder.equal(root.get("furnished"), furnished);
    }

    /**
     * Method isRenovated is used to search flat by renovated flat or not
     * @param renovated Boolean is flat renovated(true) or not(false)
     * @return Object
     */
    public static Specification<Flat> isRenovated(Boolean renovated) {
        return (root, query, builder) ->
                renovated == null ?
                        builder.conjunction() :
                        builder.equal(root.get("renovated"), renovated);
    }
    /**
     * Method isWifi is used to search flat with existing of wifi
     * @param wifi Boolean is flat has wifi or not
     * @return Object
     */
    public static Specification<Flat> isWifi(Boolean wifi) {
        return (root, query, builder) ->
                wifi == null ?
                        builder.conjunction() :
                        builder.equal(root.get("wifi"), wifi);
    }
    /**
     * Method wifiSpeed is used to search flat with internet speed
     * @param internetSpeed Float is speed of internet in the flat
     * @return Object
     */
    public static Specification<Flat> wifiSpeed(Float internetSpeed) {
        return (root, query, builder) ->
                internetSpeed == null ?
                        builder.conjunction() :
                        builder.greaterThan(root.get("internetSpeed"), internetSpeed);
    }

    /**
     * Method availableFrom is used to search flat with date when is available
     * @param availableFrom String is date when flat is available
     * @return Object
     */
    public static Specification<Flat> availableFrom(LocalDate availableFrom) {
        return (root, query, builder) ->
                availableFrom == null ?
                        builder.conjunction() :
                        builder.greaterThan(root.get("availableFrom"), availableFrom);
    }
}
