package com.timon.rhouse.controller;


import com.timon.rhouse.domain.Flat;
import com.timon.rhouse.repository.FlatRepository;
import com.timon.rhouse.service.FileService;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;


@RestController
public class FileApi {

    @Autowired
    private FileService storageService;

    @Autowired
    private FlatRepository flatRepository;

    /**
     * FileApi empty Constructor
     */
    public FileApi() {
    }




    /**
     * REST API endpoint - /upload
     * Method: POST
     * Method uploadFile is used to save file for a flat , required 2 parameters, id - is of Flat and file - is any image file
     * Method requires 2 RequestParam - MultipartFile file and Long id
     * @param file is MultipartFile file
     * @param id is id of Flat where file haas to be stored
     * @param attributes is redirect attribute
     * @return HttpStatus if created well - 200 if not status 500
     * @throws Exception if file size is bigger than 500000kb
     */

    @PostMapping("/upload")
    public RedirectView uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("id") Long id, RedirectAttributes attributes) throws Exception {

        attributes.addFlashAttribute("flashAttribute", "redirectWithRedirectView");
        attributes.addAttribute("attribute", "redirectWithRedirectView");
        long f= file.getSize();
        if(f>500000){
            throw new Exception("Too large file");

        }else{
            Flat flat= flatRepository.findById(id).get();
            try {
                storageService.store(file,flat);
                return new RedirectView("http://localhost:8080/WelcomePage.jsf");

            } catch (Exception e) {
                return new RedirectView("http://localhost:8080/error.jsf");

            }
        }

    }

}
