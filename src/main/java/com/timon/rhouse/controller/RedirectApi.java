package com.timon.rhouse.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;


@RestController
public class RedirectApi {


    /**
     * Redirectview just redirect page from "/" to "WelcomePage.jsf"
     * @param attributes - is not required attribute
     * @return new RedirectView which redirects to WelcomePage.jsf
     */
    @GetMapping("/")
    public RedirectView redirectWithUsingRedirectView(
            RedirectAttributes attributes) {
        attributes.addFlashAttribute("flashAttribute", "redirectWithRedirectView");
        attributes.addAttribute("attribute", "redirectWithRedirectView");
        return new RedirectView("http://localhost:8080/WelcomePage.jsf");
    }




}
