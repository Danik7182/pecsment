package com.timon.rhouse.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.*;
import org.springframework.security.config.annotation.authentication.builders.*;
import org.springframework.security.config.annotation.web.builders.*;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public UserDetailsService userDetailsService() {
        return new ImplUserDetailsService();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());

        return authProvider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/landlord/**").access("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")

                .antMatchers("/").permitAll()
                .antMatchers("/error.jsf").permitAll()
                .antMatchers("/flat/**").permitAll()
                .antMatchers("/consumer/**").permitAll()
                .antMatchers("/photos/**").permitAll()
                .antMatchers("/addPicture.jsf").permitAll()
                .antMatchers("/WelcomePage.jsf").permitAll()
                .antMatchers("/login.jsf").access("hasRole('ROLE_ANONYMOUS')")
                .antMatchers("/login.xhtml").access("hasRole('ROLE_ANONYMOUS')")
                .antMatchers("/login").access("hasRole('ROLE_ANONYMOUS')")
                .antMatchers("/searchPage.jsf").permitAll()
                .antMatchers("/search.jsf").permitAll()
                .antMatchers("/createFlat.jsf").authenticated()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/").permitAll()
                .antMatchers("/registrationChoose.jsf").permitAll()
                .antMatchers("/registrationConsumer.jsf").permitAll()
                .antMatchers("/registrationLandlord.jsf").permitAll()

                .antMatchers("/flatProfile.jsf").permitAll()
                .antMatchers("/profConsumerPage.jsf").authenticated()
                .and()
                .formLogin().loginPage("/login.jsf").loginProcessingUrl("/login")
                .defaultSuccessUrl("/WelcomePage.jsf")
                .failureForwardUrl("/error.jsf")

                .and()
                .logout()
                .logoutUrl("/logout")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/WelcomePage.jsf")

                .and()
                .logout().permitAll()
                .and()
                .rememberMe().key("uniqueAndSecret").tokenValiditySeconds(86400);

        http.csrf().disable();

    }
}