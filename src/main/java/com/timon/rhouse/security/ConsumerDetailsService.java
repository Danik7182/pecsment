package com.timon.rhouse.security;

import com.timon.rhouse.domain.Consumer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;

public class ConsumerDetailsService implements UserDetails {

    /**
     * Consumer entity
     */
    Consumer consumer;

    /**
     * ConsumerDetailsService with Consumer parameter
     * @param consumer Consumer object
     */
    public ConsumerDetailsService(Consumer consumer) {
        this.consumer = consumer;
    }

    /**
     * Overriding  getAuthorities from interface UserDetails
     * @return list of authorities from consumers entity
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(consumer.getRoles());
        return Arrays.asList(authority);
    }

    /**
     * Overriding getPassword
     * @return password of Consumer
     */
    @Override
    public String getPassword() {
        return consumer.getPassword();
    }

    /**
     * Overriding getUsername
     * @return username of Consumer
     */
    @Override
    public String getUsername() {
        return consumer.getUsername();
    }
    /**
     * Overriding isAccountNonExpired
     * @return true
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Overriding isAccountNonLocked
     * @return true
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Overriding isCredentialsNonExpired
     * @return true
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Overriding isEnabled
     * @return true
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}
