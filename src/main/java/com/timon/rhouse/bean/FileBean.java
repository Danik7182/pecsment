package com.timon.rhouse.bean;

import com.timon.rhouse.domain.File;
import com.timon.rhouse.domain.Flat;
import com.timon.rhouse.repository.FileRepository;
import com.timon.rhouse.repository.FlatRepository;
import com.timon.rhouse.service.FileService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;


import javax.faces.bean.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



@ApplicationScoped
@Component(value = "fileJsfService")
@ELBeanName(value = "fileJsfService")
public class FileBean {



    @Autowired
    private FileRepository fileRepository;
    @Autowired
    private FlatRepository flatRepository;


    @Autowired
    private FileService storageService;


    private final static Logger LOG = LogManager.getLogger();

    /**
     * Method getImage() is used to get  one image  of flat from database
      * @return DefaultStreamedContent - image
     * @throws IOException if program offcurs witll throw IOException
     */
    public StreamedContent getImage() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        try{
            if(context.getCurrentPhaseId()==PhaseId.RENDER_RESPONSE){
                return new DefaultStreamedContent();
            }else{
                String flatid = context.getExternalContext().getRequestParameterMap().get("fileId");
                File file = fileRepository.findById(Long.valueOf(flatid)).get();
                byte[] data=file.getData();

                return DefaultStreamedContent.builder().contentType(file.getType()).name(file.getFileName())
                        .stream( ()->new ByteArrayInputStream(data)).build();

            }
        }catch (Exception e){
            LOG.error(e);
            throw  new IOException("Can't find picture ");

        }


    }


    /**
     * Method getImages is used to get all ids of filee
     * @param id is id of Flat
     * @return list of Flat ids
     */
    public List<String> getImages(long id){
        Flat flat = flatRepository.findById(id).get();
        List<File> files = flat.getFiles();
        List<String> ids = new ArrayList<String>();
        for(File file:files)
            ids.add(file.getId().toString());
        return ids;
    }


  public String deleteImage(long id){
        fileRepository.deleteById(id);
        return "/editFlat.jsf?faces-redirect=true";
  }
}
