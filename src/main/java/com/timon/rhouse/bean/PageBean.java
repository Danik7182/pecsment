package com.timon.rhouse.bean;

import com.timon.rhouse.domain.Landlord;
import com.timon.rhouse.repository.ConsumerRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Scope(value = "session")
@Component(value = "pageJsfService")
@ELBeanName(value = "pageJsfService")
public class PageBean {


    /**
     * Logger from log4j
     */
    private final static Logger LOG = LogManager.getLogger();

    @Autowired
    ConsumerRepository consumerRepository;

    /**
     * Method isUser used to check if user logged in or not
     * @return Boolean if true - User logged in,false - User not logged in
     * @throws Exception  if error occurs writing  exception message  into LOG and throws new Exception with message -"Can't find its user or not"
     */
    public Boolean isUser() throws Exception{

        try{
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Object name = auth.getPrincipal();
            if(name=="anonymousUser"){
                return false;
            }else {
                return true;
            }

        }catch (Exception e){

            LOG.error(e);
            throw new Exception("Can't find its user or not");
        }

    }



    /**
     * Method isConsumer used to find if logged in user is consumer or not
     * username of Consumer took from SecurityContextHolder and with this username check if consumer in db exist or not, if exist it's consumer if not it's landlord user
     *
     * @return Boolean - if true - User is consumer if not User is Landlord
     * @throws Exception  if error occurs writing  exception message  into LOG and throws new Exception with message -"Can't check if its consumer"
     */
    public Boolean isConsumer() throws Exception{
        try{

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Object name = auth.getPrincipal();
            String username;
            if (name instanceof UserDetails) {
                username = ((UserDetails)name).getUsername();
            } else {
                username = name.toString();
            }
            if(consumerRepository.findByUsername(username)!=null){
                return true;
            }else {
                return false;
            }
        }catch (Exception e){
            LOG.error(e);
            throw  new Exception("Can't check if its consumer");
        }

    }

    /**
     * Method logout is used to logout user , clearing  SecurityContextHolder context
     * @return returning String which redirecting page to WelcomePage.jsf
     */
    public String logout(){
        SecurityContextHolder.clearContext();
        return "/WelcomePage.jsf?faces-redirect=true";

    }




}
