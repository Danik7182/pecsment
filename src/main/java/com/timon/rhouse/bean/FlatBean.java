package com.timon.rhouse.bean;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.timon.rhouse.controller.specification.FlatSpec;
import com.timon.rhouse.domain.File;
import com.timon.rhouse.domain.Flat;
import com.timon.rhouse.domain.Landlord;
import com.timon.rhouse.repository.FlatRepository;
import com.timon.rhouse.repository.LandlordRepository;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javamoney.moneta.FastMoney;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.money.Monetary;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Scope(value = "session")
@Component(value = "flatJsfService")
@ELBeanName(value = "flatJsfService")
public class FlatBean {
// is an annotation provided by Rewrite that configures the name of the bean on its scope.

    /**
     * Logger from log4j
     */
    private final static Logger LOG = LogManager.getLogger();

    /**
     * Id for Flat.
     * Type:long
     */
    private long id;
    /**
     * Title for flat
     * Type:String
     */
    private String title;
    /**
     * City name for flat
     * Type:String
     */
    private String city;
    /**
     * Address for flat
     * Type:String
     */
    private String address;
    /**
     * Price for flat
     * Type:FastMoney object
     */
    private FastMoney price;
    /**
     * Utilities for flat
     * Type:FastMoney object
     */
    private FastMoney utilities;
    /**
     * Details for flat
     * Type:String
     */
    private String details;
    /**
     * Rent or Buy for flat. Rent -true,Buy - false
     * Type:Boolean
     */
    private Boolean rentORbuy;
    /**
     * List of Files for flat
     * Type:List<File>
     */
    private List<File> files;
    /**
     * Landlord  for flat
     * Type:Landlord object
     */
    private Landlord landlord;
    /**
     * Number of rooms for flat
     * Type:Integer
     * Integer because for input box it will show empty String
     */
    private Integer numberOfRooms;
    /**
     * Number of Beds for flat
     * Type:Integer
     * Integer because for input box it will show empty String
     */
    private Integer numberOfBeds;
    /**
     * Wifi for Flat, if wifi exist or not
     * Type:Boolean
     */
    private Boolean wifi;
    /**
     * Renovated for flat, flat is renovated or not
     * Type:Boolean
     */
    private Boolean renovated;
    /**
     * Furnished for flat, flat is furnished or not
     * Type:Boolean
     */
    private Boolean furnished;
    /**
     * Internet speed for flat
     * Type:Float
     */
    private Float internetSpeed;
    /**
     * Available from for flat, date when is flat available
     * Type:LocalDate
     */
    private LocalDate availableFrom;
    /**
     * Utilities for flat
     * Type:BigDecimal
     * in decimal because in front-end easier just write BigDecimal and in backend it will create FastMoney
     */
    private BigDecimal utilitiesDecimal;
    /**
     * Id of Landlord for flat;
     * Type:long
     */
    private long landlordid;
    /**
     * Rent or Buy for flat. Rent -true,Buy - false
     * Type:Boolean
     */
    private BigDecimal priceDecimal;
    /**
     * Empty bigDecimal is used to restore utilitiesDecimal/priceDecimal
     * Type:BigDecimal
     */
    private BigDecimal empty;


    private String cityname;
    private int priceLess;
    private int priceGreater;
    private int untilityLess;
    private int untilityGreater;
    private int roomsLess;
    private int roomsGreater;

    @Autowired
    LandlordRepository landlordRepository;

    @Autowired
    FlatRepository flatRepository;


    /**
     * List of Flats is for bean, to store list of flats
     * Type: List Flat
     */
    private List<Flat> listOfFlats;


    /**
     * Method loadData with 2 parameters: pagenumber and size.
     * Method is filling {@link FlatBean#listOfFlats} with list of flats from databse
     * @param pagenumber - is number of pages need to be taken from database
     * @param size - is count of entites to be taken from database
     * @throws Exception if any error occur it will write to Logger and throw new Exception with message
     */
    public void loadData(int pagenumber,int size) throws Exception {
        try{
            Pageable firstPageWithFive = PageRequest.of(pagenumber, size);

            listOfFlats=flatRepository.findAll(firstPageWithFive).getContent();


        }catch (Exception e ){
            LOG.error(e);
            throw  new Exception("Can't load page");
        }
    }

    /**
     * Method openFlatPage with 1 parameter
     * Mehtod is used to redirect to page flatProfile.jsf and putting into sessionMap FLAT object
     * @param id is id of Flat which has to be stored  into session map
     * @return is  string redirecting to flatProfile.jsf
     * @throws Exception if error occurs will write into LOG and throw nex Exception with message
     */

    public String openFlatPage(long id) throws Exception {

        try {
            Flat flat = this.flatRepository.findById(id).get();

            Map<String, Object> sessionMap= FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
            sessionMap.put("flat",flat);
            int d=0;
            return "/flatProfile.jsf?faces-redirect=true";


        }catch (Exception e){
            LOG.error(e);
            throw new Exception("Can't redirect to page");
        }



    }


    /**
     * Method searchFlat is for advance search
     * Method using Specifications
     * Need to fill bean's variables to use method
     * Method fills variable {@link FlatBean#listOfFlats}
     */
    public void searchFlat(){


        FastMoney priceLess1=null;
        FastMoney priceGreater1=null;
        FastMoney untilityLess1=null;
        FastMoney untilityGreater1=null;
        Integer roomsLess1=null;
        Integer roomsGreater1=null;

        if(priceGreater!=0){
            priceLess1=FastMoney.of(priceLess, Monetary.getCurrency("EUR"));
            priceGreater1=FastMoney.of(priceGreater, Monetary.getCurrency("EUR"));
        }

        if(untilityGreater!=0){
            untilityLess1=FastMoney.of(untilityLess, Monetary.getCurrency("EUR"));
            untilityGreater1=FastMoney.of(untilityGreater, Monetary.getCurrency("EUR"));

        }
        if(roomsLess!=0 || roomsGreater!=0){
            roomsLess1=roomsLess;
        }
    if(roomsGreater!=0){
        roomsGreater1=roomsGreater;
    }

    if(city==""){
        city=null;
    }

        Specification spec1 = FlatSpec.cityNameEquals(city);
        Specification spec2 = FlatSpec.priceBetween(priceLess1,priceGreater1);
        Specification spec3 = FlatSpec.utilitiesBetween(untilityLess1,untilityGreater1);
        Specification spec4 = FlatSpec.rentOrBuy(rentORbuy);
        Specification spec5 = FlatSpec.isRenovated(renovated);
        Specification spec6 = FlatSpec.isFurnished(furnished);
        Specification spec7 = FlatSpec.roomsBetween(roomsLess1,roomsGreater1);
        Specification spec8 = FlatSpec.isWifi(wifi);
        Specification spec10 = FlatSpec.availableFrom(availableFrom);

//        Specification spec = Specification.where(spec1);
        Specification spec = Specification.where(spec1).and(spec2).and(spec3)
                .and(spec4).and(spec5).and(spec6).and(spec7).and(spec8).and(spec10);

        this.listOfFlats=flatRepository.findAll(spec);

    }
    /**
     * Method createFlat with 1 parameter
     * Method used to create new flat aand store into database
     * @param flatBean is FlatBean which stores all parameter of Flat
     * @return returning string which redirect to filesave.jsf page where need to save new picture
     */
    public String createFlat(FlatBean flatBean){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object name = auth.getPrincipal();
        String username;
        if (name instanceof UserDetails) {
            username = ((UserDetails)name).getUsername();
        } else {
            username = name.toString();
        }
        Landlord landlord=landlordRepository.findByUsername(username);

        Flat f1 = new Flat(flatBean.title, flatBean.city, flatBean.address, flatBean.priceDecimal,
                flatBean.utilitiesDecimal, flatBean.details,flatBean.numberOfRooms,flatBean.numberOfBeds,
                flatBean.wifi,flatBean.internetSpeed,flatBean.renovated,flatBean.furnished,flatBean.rentORbuy,"EUR",landlord,flatBean.availableFrom);


        flatRepository.save(f1);
        cleareVariables();
        flatBean.setId(f1.getId());
        return "/filesave.jsf?faces-reidrect=true";

    }


    /**
     * Setting null all variables which used in creating Flat , so when page is opened it again will be empty
     */
    public void cleareVariables(){
        setTitle(null);
        setCity(null);
        setAddress(null);
        setPriceDecimal(null);
        setUtilitiesDecimal(null);
        setPrice(null);
        setDetails(null);
        setNumberOfBeds(null);
        setNumberOfRooms(null);
        setWifi(null);
        setInternetSpeed(null);
        setRenovated(null);
        setFurnished(null);
        setRentORbuy(null);
        setAvailableFrom(null);
    }

    public String recirectToMainPage(){
        setTitle(null);
        setCity(null);
        setAddress(null);
        setPriceDecimal(null);
        setUtilitiesDecimal(null);
        setPrice(null);
        setDetails(null);
        setNumberOfBeds(null);
        setNumberOfRooms(null);
        setWifi(null);
        setInternetSpeed(null);
        setRenovated(null);
        setFurnished(null);
        setRentORbuy(null);
        setAvailableFrom(null);
        return "/WelcomePage.jsf?faces-reidrect=true";
    }
    /**
     * Method change with 1 parameter
     * Method is used to put flat into session map and redirect to editFlat.jsf
     * @param id is flat id which has to be putted to session map
     * @return returning string which redirect to edtiFlat.jsf
     */
    public String change(Long id){
        try {
            Flat flat = this.flatRepository.findById(id).get();

            Map<String, Object> sessionMap= FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
            sessionMap.put("editRecord",flat);
            return "/editFlat.jsf?faces-reidrect=true";


        }catch (Exception e){
            return null;
        }
    }


    /**
     * Method save with 3 parameters
     * Method used to update flat information
     * @param price is BigDecimal price number of flat
     * @param utilities is BigDecimal utilities number of flat
     * @param flat - Flat object with all data
     * @return returning string which redirect to profPage.jsf
     */
    public String save(BigDecimal price,BigDecimal utilities,Flat flat){
        if(price !=null){
            flat.setPrice(FastMoney.of(price, Monetary.getCurrency("EUR")));
        }
        if(utilities!=null){
            flat.setUtilities(FastMoney.of(utilities, Monetary.getCurrency("EUR")));
        }


        flatRepository.save(flat);
        this.priceDecimal= this.empty;
        this.utilitiesDecimal=this.empty;

        return "/profPage.jsf?faces-reidrect=true";


    }


    /**
     * getter for {@link FlatBean#id}
     * @return long id
     */
    public long getId() {
        return id;
    }


    /**
     * setter for {@link FlatBean#id}
     * @param id is setting id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * getter for {@link FlatBean#listOfFlats}
     * @return list of Flats
     */
    public List<Flat> getListOfFlats() {
        return listOfFlats;
    }

    /**
     * setter for {@link FlatBean#listOfFlats}
     * @param listOfFlats is setting id
     */
    public void setListOfFlats(List<Flat> listOfFlats) {
        this.listOfFlats = listOfFlats;
    }

    /**
     * getter for {@link FlatBean#title}
     * @return String title
     */
    public String getTitle() {
        return title;
    }

    /**
     * setter for {@link FlatBean#title}
     * @param title is setting title
     */
    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * getter for {@link FlatBean#city}
     * @return String city name
     */
    public String getCity() {
        return city;
    }

    /**
     * setter for {@link FlatBean#city}
     * @param city is setting city's name
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * getter for {@link FlatBean#address}
     * @return String address
     */
    public String getAddress() {
        return address;
    }

    /**
     * setter for {@link FlatBean#address}
     * @param address is setting address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * getter for {@link FlatBean#price}
     * @return FaastMoney object price
     */
    public FastMoney getPrice() {
        return price;
    }

    /**
     * setter for {@link FlatBean#price}
     * @param price is setting object FastMoney price
     */
    public void setPrice(FastMoney price) {
        this.price = price;
    }

    /**
     * getter for {@link FlatBean#utilities}
     * @return FastMoney object utilities
     */
    public FastMoney getUtilities() {
        return utilities;
    }

    /**
     * setter for {@link FlatBean#utilities}
     * @param utilities is setting FastMoney object utilities
     */
    public void setUtilities(FastMoney utilities) {
        this.utilities = utilities;
    }



    /**
     * getter for {@link FlatBean#details}
     * @return String detials
     */
    public String getDetails() {
        return details;
    }

    /**
     * setter for {@link FlatBean#details}
     * @param details is setting details of flat
     */
    public void setDetails(String details) {
        this.details = details;
    }

    /**
     * getter for {@link FlatBean#rentORbuy}
     * @return Boolean rent or buy (true -rent, false - buy)
     */
    public Boolean getRentORbuy() {
        return rentORbuy;
    }

    /**
     * setter for {@link FlatBean#rentORbuy}
     * @param rentORbuy is setting renting(true) flat or buying(false)
     */
    public void setRentORbuy(Boolean rentORbuy) {
        this.rentORbuy = rentORbuy;
    }

    /**
     * getter for {@link FlatBean#getFiles()}
     * @return List of Files
     */
    public List<File> getFiles() {
        return files;
    }

    /**
     * setter for {@link FlatBean#files}
     * @param files is setting List of files
     */
    public void setFiles(List<File> files) {
        this.files = files;
    }

    /**
     * getter for {@link FlatBean#landlord}
     * @return Landlord object landlord
     */
    public Landlord getLandlord() {
        return landlord;
    }

    /**
     * setter for {@link FlatBean#landlord}
     * @param landlord is setting Landlord object
     */
    public void setLandlord(Landlord landlord) {
        this.landlord = landlord;
    }

    /**
     * getter for {@link FlatBean#numberOfRooms}
     * @return Integer number of rooms
     */
    public Integer getNumberOfRooms() {
        return numberOfRooms;
    }

    /**
     * setter for {@link FlatBean#numberOfRooms}
     * @param numberOfRooms is setting number of Rooms of flat
     */
    public void setNumberOfRooms(Integer numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    /**
     * getter for {@link FlatBean#numberOfBeds}
     * @return Integer number of rooms
     */
    public Integer getNumberOfBeds() {
        return numberOfBeds;
    }

    /**
     * setter for {@link FlatBean#numberOfBeds}
     * @param numberOfBeds is setting number of beds for flat
     */
    public void setNumberOfBeds(Integer numberOfBeds) {
        this.numberOfBeds = numberOfBeds;
    }

    /**
     * getter for {@link FlatBean#wifi}
     * @return Boolean witi is exist or not
     */
    public Boolean getWifi() {
        return wifi;
    }

    /**
     * setter for {@link FlatBean#wifi}
     * @param wifi is setting wifi existing
     */
    public void setWifi(Boolean wifi) {
        this.wifi = wifi;
    }


    /**
     * getter for {@link FlatBean#renovated}
     * @return Boolean renovated flat or not
     */
    public Boolean getRenovated() {
        return renovated;
    }

    /**
     * setter for {@link FlatBean#renovated}
     * @param renovated is setting for flat renovated or not
     */
    public void setRenovated(Boolean renovated) {
        this.renovated = renovated;
    }

    public Boolean getFurnished() {
        return furnished;
    }

    /**
     * getter for {@link FlatBean#furnished}
     * @return Boolean flat is furnished or not
     */
    public Boolean isFurnished() {
        return furnished;
    }

    /**
     * setter for {@link FlatBean#furnished}
     * @param furnished is setting furnished flat or not
     */
    public void setFurnished(Boolean furnished) {
        this.furnished = furnished;
    }

    /**
     * getter for {@link FlatBean#internetSpeed}
     * @return Float speed of internet
     */
    public Float getInternetSpeed() {
        return internetSpeed;
    }

    /**
     * setter for {@link FlatBean#internetSpeed}
     * @param internetSpeed is setting speed of internet
     */
    public void setInternetSpeed(Float internetSpeed) {
        this.internetSpeed = internetSpeed;
    }

    /**
     * getter for {@link FlatBean#availableFrom}
     * @return LocalDate date when flat available
     */
    public LocalDate getAvailableFrom() {
        return availableFrom;
    }

    /**
     * setter for {@link FlatBean#availableFrom}
     * @param availableFrom is setting date when flat is Available
     */
    public void setAvailableFrom(LocalDate availableFrom) {
        this.availableFrom = availableFrom;
    }

    /**
     * getter for {@link FlatBean#utilitiesDecimal}
     * @return is setting Flat utilities in decimal
     */
    public BigDecimal getUtilitiesDecimal() {
        return utilitiesDecimal;
    }

    /**
     * setter for {@link FlatBean#utilitiesDecimal}
     * @param utilitiesDecimal is setting decimal number of utilities
     */
    public void setUtilitiesDecimal(BigDecimal utilitiesDecimal) {
        this.utilitiesDecimal = utilitiesDecimal;
    }

    /**
     * getter for {@link FlatBean#landlordid}
     * @return long Landlord's id
     */
    public long getLandlordid() {
        return landlordid;
    }

    /**
     * setter for {@link FlatBean#landlordid}
     * @param landlordid is setting id of landlord
     */
    public void setLandlordid(long landlordid) {
        this.landlordid = landlordid;
    }

    /**
     * getter for {@link FlatBean#priceDecimal}
     * @return BigDecimal price in decimal number
     */
    public BigDecimal getPriceDecimal() {
        return priceDecimal;
    }

    /**
     * setter for {@link FlatBean#priceDecimal}
     * @param priceDecimal is setting price in decimal number
     */
    public void setPriceDecimal(BigDecimal priceDecimal) {
        this.priceDecimal = priceDecimal;
    }

    /**
     * getter for {@link FlatBean#cityname}
     * @return String cityname is name of City
     */
    public String getCityname() {
        return cityname;
    }

    /**
     * setter for {@link FlatBean#cityname}
     * @param cityname is setting name in string
     */
    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    /**
     * getter for {@link FlatBean#priceLess}
     * @return int priceLess
     */
    public int getPriceLess() {
        return priceLess;
    }

    /**
     * setter for {@link FlatBean#priceLess}
     * @param priceLess is setting price in int
     */
    public void setPriceLess(int priceLess) {
        this.priceLess = priceLess;
    }

    /**
     * getter for {@link FlatBean#priceGreater}
     * @return BigDecimal price in decimal number
     */
    public int getPriceGreater() {
        return priceGreater;
    }

    /**
     * setter for {@link FlatBean#priceGreater}
     * @param priceGreater is setting price in int
     */
    public void setPriceGreater(int priceGreater) {
        this.priceGreater = priceGreater;
    }

    /**
     * getter for {@link FlatBean#untilityLess}
     * @return BigDecimal price in decimal number
     */
    public int getUntilityLess() {
        return untilityLess;
    }

    /**
     * setter for {@link FlatBean#untilityLess}
     * @param untilityLess is setting utilities in int
     */
    public void setUntilityLess(int untilityLess) {
        this.untilityLess = untilityLess;
    }

    /**
     * getter for {@link FlatBean#untilityGreater}
     * @return BigDecimal price in decimal number
     */
    public int getUntilityGreater() {
        return untilityGreater;
    }

    /**
     * setter for {@link FlatBean#untilityGreater}
     * @param untilityGreater is setting utilities in int
     */
    public void setUntilityGreater(int untilityGreater) {
        this.untilityGreater = untilityGreater;
    }

    /**
     * getter for {@link FlatBean#roomsLess}
     * @return BigDecimal price in decimal number
     */
    public Integer getRoomsLess() {
        return roomsLess;
    }

    /**
     * setter for {@link FlatBean#roomsLess}
     * @param roomsLess is setting rooms in int
     */
    public void setRoomsLess(Integer roomsLess) {
        this.roomsLess = roomsLess;
    }

    /**
     * getter for {@link FlatBean#roomsGreater}
     * @return BigDecimal price in decimal number
     */
    public Integer getRoomsGreater() {
        return roomsGreater;
    }

    /**
     * setter for {@link FlatBean#roomsGreater}
     * @param roomsGreater is setting rooms in int
     */
    public void setRoomsGreater(Integer roomsGreater) {
        this.roomsGreater = roomsGreater;
    }

}
