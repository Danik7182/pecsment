package com.timon.rhouse.bean;

import com.timon.rhouse.domain.File;
import com.timon.rhouse.domain.Flat;
import com.timon.rhouse.repository.FileRepository;
import com.timon.rhouse.repository.FlatRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ocpsoft.rewrite.annotation.RequestAction;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.ocpsoft.rewrite.faces.annotation.Deferred;
import org.ocpsoft.rewrite.faces.annotation.IgnorePostback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import com.timon.rhouse.domain.Landlord;
import com.timon.rhouse.repository.LandlordRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Scope(value = "session")
@Component(value = "landlordJsfService")
@ELBeanName(value = "landlordJsfService")
public class LandlordBean {

    private final static Logger LOG = LogManager.getLogger();

    @Autowired
    LandlordRepository landlordRepository;

    @Autowired
    FlatRepository flatRepository;

    /**
     * flatList is all list of flat
     * Type: List<Flat>
     */
    private List<Flat> flatList;
    /**
     * landlordsFlatList is all list of flat for specific Landlord
     * Type: List<Flat>
     */
    private List<Flat> landlordsFlatList;


    /**
     * Username for landlord
     * Type: String
     */
    private String username;
    /**
     * Name for landlord
     * Type: String
     */
    private String name;
    /**
     * Surname for landlord
     * Type: String
     */
    private String surname;
    /**
     * Email for landlord
     * Type: String
     */
    private String email;
    /**
     * Contact Number for landlord
     * Type: String
     */
    private String contactNumber;
    /**
     * Password for landlord
     * Type: String
     */
    private String password;


    /**
     * Method getLoggedUser is to find which user is Logged in.
     * User's username found from SecurityContextHolder and from that searching user from database
     * @return object Landlord
     * @throws Exception  if error occurs writing  exception message  into LOG and throws new Exception with message -"Can't find landlord"
     */
    public Landlord getLoggedUser() throws Exception {

        try{

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Object name = auth.getPrincipal();
            String username;
            if (name instanceof UserDetails) {
                username = ((UserDetails)name).getUsername();
            } else {
                username = name.toString();
            }
            Landlord landlord=landlordRepository.findByUsername(username);

            this.flatList= flatRepository.findAllByLandlord(landlord);
            return landlord;
        } catch (Exception e){

            LOG.error(e);
            throw new Exception("Can't find landlord");
        }

    }


    /**
     * Method loadData is used to fill {@link LandlordBean#flatList} with all flats from database
     * @param pagenumber - is number of pages need to be taken from database
     * @param size - is count of entities to be taken from database
     * @throws Exception  if error occurs writing  exception message  into LOG and throws new Exception with message -"Can't load flats"
     */
    @Deferred
    @RequestAction
    @IgnorePostback
    public void loadData(int pagenumber,int size) throws Exception {
        try{
            Pageable firstPageWithFive = PageRequest.of(pagenumber, size);

            flatList=flatRepository.findAll(firstPageWithFive).getContent();

        }catch (Exception e ){
            LOG.error(e);
            throw  new Exception("Can't load flats");
        }
    }

    /**
     * Method createNewLandlord with 1 parameter
     * Method used to create new Landlord and into database
     * @param landlord is LandlordBean with all parameters of object Landlord
     * @return redirect to pages, if save successfully to  WelcomePage.jsf else to registrationLandlord.jsf page again
     */
    public String createNewLandlord(LandlordBean landlord){
        try{
            landlordRepository.save(new Landlord(landlord.getUsername(), landlord.getName(),landlord.getSurname(), landlord.getEmail(),landlord.getContactNumber(),landlord.getPassword(), new BigDecimal(0),"EUR","ROLE_USER"));
            clearAllFields();
            return "/WelcomePage.jsf?faces-redirect=true";
        }catch (Exception e){
            LOG.error( "Can't create new Landlord " + e);
            return "/registrationLandlord.jsf?faces-redirect=true";
        }

    }

    /**
     * Clearing all variables. Used to clear all inputs after registration.
     */
    public void clearAllFields(){
        setUsername(null);
        setName(null);
        setSurname(null);
        setEmail(null);
        setPassword(null);
        setContactNumber(null);
    }

    /**
     * Method getFirstLetterOfName is used to get first letter of Landlord who is logged in
     * Method gets username of landlord from SecurityContextHolder and searches from db to get landlord's whole object
     * @return String with first letter of Landlord
     * @throws Exception  if error occurs writing  exception message  into LOG and throws new Exception with message -"Can't get first letter"
     */
    public String getFirstLetterOfName() throws Exception{
        try{

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Object name = auth.getPrincipal();
            String username;
            if (name instanceof UserDetails) {
                username = ((UserDetails)name).getUsername();
            } else {
                username = name.toString();
            }
            Landlord landlord=landlordRepository.findByUsername(username);


            return Character.toString(landlord.getName().charAt(0));
        }catch (Exception e){

            LOG.error(e);
            throw  new Exception("Can't get first letter");
        }

    }


    /**
     * Method redirectToLandlordRegistration is used to redirect to registrationLandlord.jsf page
     * @return String with redirecting into registrationLandlord.jsf page
     */
    public String redirectToLandlordRegistration(){
        return "/registrationLandlord.jsf?faces-redirect=true";
    }

    /**
     * Method loadMyFlats with 1 parameter is used to fill {@link LandlordBean#landlordsFlatList} with flats of specifing landlord
     * @param id is id of landlord  used to find all flats of this landlord
     */
    public void loadMyFlats(long id){
        Landlord landlord =landlordRepository.findById(id).get();
        List<Flat> flats= flatRepository.findAllByLandlord(landlord);

        setLandlordsFlatList(flats);

    }




    /**
     * getter for {@link LandlordBean#username}
     * @return String  username of landlord
     */
    public String getUsername() {
        return username;
    }

    /**
     * setter for {@link LandlordBean#username}
     * @param username is setting username of landlord
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * getter for {@link LandlordBean#name}
     * @return String  name of landlord
     */
    public String getName() {
        return name;
    }


    /**
     * setter for {@link LandlordBean#name}
     * @param name is setting name of landlord
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter for {@link LandlordBean#surname}
     * @return String  surname of landlord
     */
    public String getSurname() {
        return surname;
    }


    /**
     * setter for {@link LandlordBean#surname}
     * @param surname is setting surname of landlord
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * getter for {@link LandlordBean#email}
     * @return String  email of landlord
     */
    public String getEmail() {
        return email;
    }


    /**
     * setter for {@link LandlordBean#email}
     * @param email email setting email of landlord
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * getter for {@link LandlordBean#contactNumber}
     * @return String  contactNumber of landlord
     */
    public String getContactNumber() {
        return contactNumber;
    }


    /**
     * setter for {@link LandlordBean#contactNumber}
     * @param contactNumber is setting contactNumber of landlord
     */
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    /**
     * getter for {@link LandlordBean#password}
     * @return String  password of landlord
     */
    public String getPassword() {
        return password;
    }


    /**
     * setter for {@link LandlordBean#password}
     * @param password is setting password of landlord
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * getter for {@link LandlordBean#flatList}
     * @return  list of all flats
     */

    public List<Flat> getFlatList() {
        return flatList;
    }



    /**
     * setter for {@link LandlordBean#flatList}
     * @param flatList is setting list of all flats
     */
    public void setFlatList(List<Flat> flatList) {
        this.flatList = flatList;
    }



    /**
     * getter for {@link LandlordBean#landlordsFlatList}
     * @return  landlordsFlatList of one landlord
     */
    public List<Flat> getLandlordsFlatList() {
        return landlordsFlatList;
    }


    /**
     * setter for {@link LandlordBean#landlordsFlatList}
     * @param landlordsFlatList is setting landlordsFlatList of one landlord
     */
    public void setLandlordsFlatList(List<Flat> landlordsFlatList) {
        this.landlordsFlatList = landlordsFlatList;
    }
}
