package com.timon.rhouse.bean;

import com.timon.rhouse.domain.Consumer;
import com.timon.rhouse.domain.Flat;
import com.timon.rhouse.domain.Landlord;
import com.timon.rhouse.repository.ConsumerRepository;
import com.timon.rhouse.repository.FlatRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Scope(value = "session")
@Component(value = "consumerJsfService")
@ELBeanName(value = "consumerJsfService")
public class ConsumerBean {

    private final static Logger LOG = LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = false;

    @Autowired
    ConsumerRepository consumerRepository;

    @Autowired
    FlatRepository flatRepository;


    /**
     * List of favourite flats for Consumer.
     * Type:list
     */
    private List<Flat> favouriteFlats;
    /**
     * Username of Consumer
     * Type:String
     */
    private String username;
    /**
     * Name of Consumer
     * Type:String
     */
    private String name;
    /**
     * Surname of Consumer
     * Type:String
     */
    private String surname;
    /**
     * Email of Consumer
     * Type:String
     */
    private String email;
    /**
     * Contact Number of Consumer
     * Type:String
     */
    private String contactNumber;
    /**
     * Password of Consumer
     * Type: String
     */
    private String password;
    /**
     * Role of Consumer
     * Type:String
     */
    private String role;


    /**
     * Method loadFavouriteFLats with empty parameters used to set {@link ConsumerBean#favouriteFlats}.
     * Because Consumer domain stores only id of flats
     * Method first take Username from Authentication principal and with repository searches for this user
     */
    public void loadFavouriteFLats(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object name = auth.getPrincipal();
        String username;
        if (name instanceof UserDetails) {
            username = ((UserDetails)name).getUsername();
        } else {
            username = name.toString();
        }

        Consumer c =consumerRepository.findByUsername(username);

        List<Long> idOfFlats =c.getFavouriteFlats();
        List<Flat> newListOfFlats=new ArrayList<>();

        for(Long l:idOfFlats){

           newListOfFlats.add(flatRepository.findById(l).get());

        }
        setFavouriteFlats(newListOfFlats);
    }

    /**
     * Mehtod createNewConsumer is used to create new user and redirect to WelcomePage if error occurs will redirect again back to registration page
     * @param consumer - is ConsumerBean which stores all parameters of new Consumer
     * @return redirect is success to WelcomePage.jsf if not to registration.jsf
     */
    public String createNewConsumer(ConsumerBean consumer){
       try{
           consumerRepository.save(new Consumer(consumer.getUsername(),consumer.getName(),consumer.getSurname(),consumer.getEmail(),consumer.getPassword(),"ROLE_USER"));
           clearRegistrationFields();
           return "/WelcomePage.jsf?faces-redirect=true";
       }
       catch (Exception e){
           LOG.error("Can't create Consumer user" + e);
           return "/registrationConsumer.jsf?faces-redirect=true";
       }
    }

    /**
     * Clearing all variables. Used to clear all inputs after registration.
     */
    public void clearRegistrationFields(){
        setUsername(null);
        setName(null);
        setSurname(null);
        setEmail(null);
        setPassword(null);
    }

    /**
     * Method addFlatToFavouriteList with 1 parameter used to add flat's id to consumers favouriteflats
     * First it  takes username from Authentication principal and with repository find by username
     * Then new id which has to add to FavouriteFlats is checking for existence in list already or not
     * if not will add to list and save consumer with repository
     * @param idofFlat is of id of flat which user wanna add to he's favourite list . type:long
     */
    public void addFlatToFavouriteList(long idofFlat){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object name = auth.getPrincipal();
        String username;
        if (name instanceof UserDetails) {
            username = ((UserDetails)name).getUsername();
        } else {
            username = name.toString();
        }


        boolean exist=false;

        Consumer consumer=consumerRepository.findByUsername(username);
        List<Long> idOfFlats =consumer.getFavouriteFlats();
        for(Long id:idOfFlats){
            if(id==idofFlat){
                exist=true;
            }
        }
        if(exist==false){
            idOfFlats.add(idofFlat);
            consumer.setFavouriteFlats(idOfFlats);
            consumerRepository.save(consumer);
        }

    }


    /**
     * Method deleteFlatFromFavouriteList with 1 parameter is used to delete flat from Favourite list of consumer and also from ConsumerBean favourite list
     * @param idofFlat is id of Flat which Consumer whats do delete from favouriteflats list
     * @return is redirect to profConsumerPage.jsf
     */
    public String deleteFlatFromFavouriteList(long idofFlat){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object name = auth.getPrincipal();
        String username;
        if (name instanceof UserDetails) {
            username = ((UserDetails)name).getUsername();
        } else {
            username = name.toString();
        }


        Consumer consumer=consumerRepository.findByUsername(username);
        List<Long> idOfFlats =consumer.getFavouriteFlats();


        for(Long id: idOfFlats){
            if(id==idofFlat){
                idOfFlats.remove(id);
                consumerRepository.save(consumer);
                List<Flat> newListOfFlats=new ArrayList<>();
                idOfFlats =consumer.getFavouriteFlats();
                for(Long l:idOfFlats){

                    newListOfFlats.add(flatRepository.findById(l).get());

                }
                setFavouriteFlats(newListOfFlats);
                return  "/profConsumerPage.jsf?faces-redirect=true";
            }

        }
        return  "/profConsumerPage.jsf?faces-redirect=true";
    }

    /**
     * Method getFirstLetterOfName is used to get first letter of User's name
     * @return String, first letter of Name
     */
    public String getFirstLetterOfName(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object name = auth.getPrincipal();
        String username;
        if (name instanceof UserDetails) {
            username = ((UserDetails)name).getUsername();
        } else {
            username = name.toString();
        }
        Consumer consumer=consumerRepository.findByUsername(username);


        return Character.toString(consumer.getName().charAt(0));
    }


    /**
     * Method getLoggedUser is used to find a logged consumer
     * Username taken from Authentication principal and will findbyUsername from repository
     * @return Consumer domain
     */
    public Consumer getLoggedUser(){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object name = auth.getPrincipal();
        String username;
        if (name instanceof UserDetails) {
            username = ((UserDetails)name).getUsername();
        } else {
            username = name.toString();
        }
        Consumer consumer=consumerRepository.findByUsername(username);

        return consumer;
    }


    /**
     * Method redirectToConsumerRegistration is used to redirect to registration of Consumer page
     * @return redirect to registrationConsumer.jsf
     */
    public String redirectToConsumerRegistration(){

        return "/registrationConsumer.jsf?faces-redirect=true";
    }


    /**
     * @return this list: {@link ConsumerBean#favouriteFlats}
     */
    public List<Flat> getFavouriteFlats() {
        return favouriteFlats;
    }

    /**
     * Setting this list :{@link ConsumerBean#favouriteFlats}
     * @param favouriteFlats is new List of Flat
     *
     */
    public void setFavouriteFlats(List<Flat> favouriteFlats) {
        this.favouriteFlats = favouriteFlats;
    }

    /**
     *
     * @return this String - {@link ConsumerBean#username}
     */
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return this String - {@link ConsumerBean#name}
     */

    public String getName() {
        return name;
    }

    /**
     *
     * @param name is a new name for {@link ConsumerBean#name}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return this String - {@link ConsumerBean#surname}
     */
    public String getSurname() {
        return surname;
    }

    /**
     *
     * @param surname is a new surname for {@link ConsumerBean#surname}
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     *
     * @return this String - {@link ConsumerBean#email}
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email is a new email for {@link ConsumerBean#email}
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return this String - {@link ConsumerBean#contactNumber}
     */
    public String getContactNumber() {
        return contactNumber;
    }

    /**
     *
     * @param contactNumber is a new email for {@link ConsumerBean#contactNumber}
     */
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    /**
     *
     * @return this String - {@link ConsumerBean#password}
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password is a new password for {@link ConsumerBean#password}
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return this String - {@link ConsumerBean#role}
     */
    public String getRole() {
        return role;
    }

    /**
     *
     * @param role is a new String for {@link ConsumerBean#role}
     */
    public void setRole(String role) {
        this.role = role;
    }
}
