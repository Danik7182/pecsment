package com.timon.rhouse.exception;

public class ResourceNotFoundException extends RuntimeException {


    /**
     *
     * Method ResourceNotFoundException is used to customize message of ResourceNotFoundException
     * @param msg is message need to send
     */
    public ResourceNotFoundException(String msg) {
        super(msg);
    }
}
