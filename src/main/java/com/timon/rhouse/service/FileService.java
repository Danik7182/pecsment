package com.timon.rhouse.service;

import com.timon.rhouse.domain.File;
import com.timon.rhouse.domain.Flat;
import com.timon.rhouse.domain.Landlord;
import com.timon.rhouse.repository.FileRepository;
import org.primefaces.shaded.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import org.apache.myfaces.custom.fileupload.UploadedFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.stream.Stream;
import javax.servlet.http.Part;

@Service
public class FileService {

    /**
     * FileRepository using
     */
    @Autowired
    private FileRepository fileDBRepository;

    /**
     * Method store used to store MultipartFile and add to file list in flat entity
     * @param file is MultipartFile
     * @param flat is Flat entity
     * @return file repository will save file to the database
     * @throws IOException if error occurs with loading file will throw IOException
     */
    public File store(MultipartFile file, Flat flat) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        File FileDB = new File(fileName, file.getContentType(), file.getBytes(),flat);

        return fileDBRepository.save(FileDB);
    }

    public File saveFile(UploadedFile file,Flat flat) throws IOException {
        String fileName = StringUtils.cleanPath(file.getName());


        InputStream initialStream = file.getInputStream();

        byte[] targetArray = IOUtils.toByteArray(initialStream);

        File FileDB = new File(fileName, file.getContentType(),targetArray ,flat);

        return fileDBRepository.save(FileDB);
    }


}
